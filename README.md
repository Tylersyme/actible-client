## Actible Client

#### Overview

This app is designed to automatically show fun places to explore in your area, and allow you to save and schedule them. Note this project is WIP.

This project also explores the possibility of creating a near native mobile experience in a SPA web application.

#### Near Native Experience

I have found that many websites, even those which are responsive for mobile devices, do not provide an equivalent user experience as a native mobile app since they "feel" different when using them and are completely non-standard. 

Native apps use the native UI widgets provided by the platform to provide a uniform experience. They follow the platform's UI standards, such as back arrow navigation (very different from back arrow navigation on web, this is only partially implemented as of now), bottom tabs (no less than 3, not more than 5), top bar title and buttons etc. I've never seen a website attempt to provide the same kind of standard experience as a native mobile app, so this project explores that option.

#### Technical Details

* This is a Blazor Webassembly application which communicates with the [Actible API](https://bitbucket.org/Tylersyme/actible-api/src/master/)
* Users sign in through Azure B2C
* Integrates with Yelp to provide the address, user reviews etc. for interesting places nearby
* Integrates with Uber to schedule a ride if you need one
* Uses Google Places API to autocomplete city names when choosing your search location

#### User Interface Preview

![](https://i.ibb.co/tzbKv1S/Capture.png)

![](https://i.ibb.co/1QX3KSp/Capture.png)

![](https://i.ibb.co/6m15rWZ/Capture.png)

![](https://i.ibb.co/3kHpTrK/Capture.png)

![](https://i.ibb.co/VvJ6kbh/Capture.png)

![](https://i.ibb.co/cv056J6/Capture.png)