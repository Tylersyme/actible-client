﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Requests
{
    public class PlaceAutocompleteRequest
    {
        public string Input { get; set; }

        public string SessionToken { get; set; }
    }
}
