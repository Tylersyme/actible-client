﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Requests
{
    public class UpdateScheduledActivityRequest
    {
        public SavedActivityDetails SavedActivityDetails { get; set; }
    }
}
