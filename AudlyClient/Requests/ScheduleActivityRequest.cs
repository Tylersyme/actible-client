﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Requests
{
    public class ScheduleActivityRequest
    {
        public string ActivityIdentifier { get; set; }

        public SavedActivityDetails SavedActivityDetails { get; set; }
    }
}
