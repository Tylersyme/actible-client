﻿using AudlyClient.Models;
using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Requests
{
    public class ActivityRequest
    {
        public List<string> Tags { get; set; } = new List<string>();

        public GeolocationCoordinates GeolocationCoordinates { get; set; }
    }
}
