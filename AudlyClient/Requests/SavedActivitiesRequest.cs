﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Requests
{
    public class SavedActivitiesRequest
    {
        /// <summary>
        /// The activity source identifiers which will not be included when retrieving activities.
        /// </summary>
        public IEnumerable<string> ExcludedActivitySourceIdentifiers { get; set; } = new List<string>();
    }
}
