﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Requests
{
    public class PlaceAutocompleteDetailsRequest
    {
        public string PlaceId { get; set; }

        public string SessionToken { get; set; }
    }
}
