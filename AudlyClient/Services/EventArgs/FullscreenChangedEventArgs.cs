﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.EventArgs
{
    public class FullscreenChangedEventArgs
    {
        public bool IsFullscreen { get; set; }
    }
}
