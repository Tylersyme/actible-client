﻿using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.EventArgs
{
    public class LocationRequestRespondedEventArgs
    {
        public bool IsSuccessful { get; set; }

        public GeolocationCoordinates Coordinates { get; set; }
    }
}
