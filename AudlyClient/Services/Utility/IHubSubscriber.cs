﻿using Easy.MessageHub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public interface IHubSubscriber
    {
        void Subscribe<T>(Action<T> callback);

        void AddSubscriptionToken(Guid token);

        void UnsubscribeAll();
    }
}
