﻿using AudlyClient.Enums;
using AudlyClient.Services.Interop.Utility;
using AudlyClient.Services.Models;
using Flurl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public class MapsService : IMapsService
    {
        private static readonly string GOOGLE_MAPS_ENDPOINT = "maps.google.com".AppendPathSegment("maps");

        private readonly IPlatformService _platformService;

        public MapsService(IPlatformService platformService)
        {
            _platformService = platformService;
        }

        public async Task<string> GetPlatformSpecificMapUrlAsync(float latitude, float longitude)
        {
            var platform = await _platformService.GetDevicePlatformAsync();

            var mapsUrl = platform.PlatformGroup == PlatformGroups.APPLE
                ? this.GetAppleMapsUrl(latitude, longitude)
                : this.GetGoogleMapsUrl(latitude, longitude);

            return mapsUrl;
        }

        private string GetAppleMapsUrl(float latitude, float longitude) =>
            this.GetMapsUrl(UrlSchemes.MAPS, latitude, longitude);

        private string GetGoogleMapsUrl(float latitude, float longitude) =>
            this.GetMapsUrl(UrlSchemes.HTTPS, latitude, longitude);

        private string GetMapsUrl(string scheme, float latitude, float longitude) =>
                $"{scheme}{GOOGLE_MAPS_ENDPOINT}"
                .SetQueryParams(new
                {
                    daddr = $"{latitude},{longitude}"
                });
    }
}
