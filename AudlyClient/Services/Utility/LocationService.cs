﻿using AudlyClient.Services.EventArgs;
using AudlyClient.Services.Interop;
using AudlyClient.Services.Interop.Models;
using AudlyClient.Services.Interop.Utility;
using Blazored.LocalStorage;
using Easy.MessageHub;
using Microsoft.IdentityModel.Tokens;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public class LocationService : ILocationService
    {
        private const string CURRENT_LOCATION_KEY = "current_position";

        private const string LAST_LOCATION_KEY = "last_position";

        private readonly ILocalStorageService _localStorageService;

        private readonly ILocationInteropService _locationInteropService;

        private readonly IMessageHub _messageHub;

        public LocationService(
            ILocalStorageService localStorageService,
            ILocationInteropService locationInteropService,
            IMessageHub messageHub)
        {
            _localStorageService = localStorageService;

            _locationInteropService = locationInteropService;

            _messageHub = messageHub;
        }

        public async Task<bool> TryGetCurrentLocationAsync(Action<GeolocationCoordinates> successAction)
        {
            if (!await _localStorageService.ContainKeyAsync(CURRENT_LOCATION_KEY))
                return false;

            var currentPosition = await _localStorageService.GetItemAsync<GeolocationCoordinates>(CURRENT_LOCATION_KEY);

            var wasFound = currentPosition != null;

            if (wasFound)
                successAction.Invoke(currentPosition);

            return wasFound;
        }

        public async Task<bool> TryGetLastLocationAsync(Action<GeolocationCoordinates> successAction)
        {
            if (!await _localStorageService.ContainKeyAsync(LAST_LOCATION_KEY))
                return false;

            var lastPosition = await _localStorageService.GetItemAsync<GeolocationCoordinates>(LAST_LOCATION_KEY);

            var wasFound = lastPosition != null;

            if (wasFound)
                successAction.Invoke(lastPosition);

            return wasFound;
        }

        public async Task<bool> TryGetBestLocationAsync(Action<GeolocationCoordinates> successAction)
        {
            var bestLocation = default(GeolocationCoordinates);

            if (await this.TryGetCurrentLocationAsync((gc) => bestLocation = gc) ||
                await this.TryGetLastLocationAsync((gc) => bestLocation = gc))
            {
                successAction.Invoke(bestLocation);

                return true;
            }

            return false;
        }

        public async Task RequestAndStoreCurrentLocationAsync()
        {
            var objectReference = DotNetObjectReference.Create(this);

            await _locationInteropService.GetGeolocationCoordinatesAsync(
                objectReference, 
                nameof(RequestedLocationSuccessCallbackAsync),
                nameof(RequestedLocationErrorCallback));
        }

        [JSInvokable]
        public async Task RequestedLocationSuccessCallbackAsync(GeolocationCoordinates geolocationCoordinates)
        {
            await this.SetCurrentLocationAsync(geolocationCoordinates);

            _messageHub.Publish(new LocationRequestRespondedEventArgs
            {
                IsSuccessful = true,
                Coordinates = geolocationCoordinates,
            });
        }

        [JSInvokable]
        public void RequestedLocationErrorCallback()
        {
            _messageHub.Publish(new LocationRequestRespondedEventArgs
            {
                IsSuccessful = false,
            });
        }

        private async Task SetCurrentLocationAsync(GeolocationCoordinates geolocationCoordinates)
        {
            await this.MoveCurrentLocationToLastLocationAsync();

            await _localStorageService.SetItemAsync(CURRENT_LOCATION_KEY, geolocationCoordinates);
        }

        private async Task MoveCurrentLocationToLastLocationAsync()
        {
            var currentPosition = default(GeolocationCoordinates);

            if (!await this.TryGetCurrentLocationAsync((gc) => currentPosition = gc))
                return;

            await _localStorageService.SetItemAsync(LAST_LOCATION_KEY, currentPosition);
        }
    }
}
