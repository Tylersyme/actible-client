﻿using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public interface ILocationService
    {
        Task<bool> TryGetCurrentLocationAsync(Action<GeolocationCoordinates> successAction);

        Task<bool> TryGetLastLocationAsync(Action<GeolocationCoordinates> successAction);

        Task<bool> TryGetBestLocationAsync(Action<GeolocationCoordinates> successAction);

        Task RequestAndStoreCurrentLocationAsync();
    }
}
