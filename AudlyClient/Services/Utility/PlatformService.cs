﻿using AudlyClient.Services.Interop.Utility;
using AudlyClient.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public class PlatformService : IPlatformService
    {
        private readonly INavigatorInteropService _navigatorInteropService;

        private Platform _devicePlatform;

        public PlatformService(INavigatorInteropService navigatorInteropService)
        {
            _navigatorInteropService = navigatorInteropService;
        }

        public async Task<Platform> GetDevicePlatformAsync()
        {
            if (_devicePlatform != null)
                return _devicePlatform;

            var platformString = await _navigatorInteropService.GetPlatformAsync();

            _devicePlatform = Platform.GetByWebPlatformName(platformString);

            return _devicePlatform;
        }
    }
}
