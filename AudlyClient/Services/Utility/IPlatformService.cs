﻿using AudlyClient.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public interface IPlatformService
    {
        Task<Platform> GetDevicePlatformAsync();
    }
}
