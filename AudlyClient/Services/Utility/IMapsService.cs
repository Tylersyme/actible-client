﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public interface IMapsService
    {
        Task<string> GetPlatformSpecificMapUrlAsync(float latitude, float longitude);
    }
}
