﻿using Easy.MessageHub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Utility
{
    public class HubSubscriber : IHubSubscriber
    {
        private IDictionary<Guid, IMessageHub> SubscriptionTokenCache { get; set; }

        private readonly IMessageHub _messageHub;

        public HubSubscriber(IMessageHub messageHub)
        {
            _messageHub = messageHub;

            this.SubscriptionTokenCache = new Dictionary<Guid, IMessageHub>();
        }

        public void Subscribe<T>(Action<T> callback)
        {
            var token = _messageHub.Subscribe(callback);

            this.AddSubscriptionToken(token);
        }

        public void AddSubscriptionToken(Guid token)
        {
            this.SubscriptionTokenCache.Add(token, _messageHub);
        }

        public void UnsubscribeAll()
        {
            foreach (var keyValue in this.SubscriptionTokenCache)
                keyValue.Value.Unsubscribe(keyValue.Key);

            this.SubscriptionTokenCache.Clear();
        }
    }
}
