﻿using AudlyClient.Services.Interop.Options;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop
{
    public interface IMDBootstrapInteropService
    {
        Task InitializeSteppersAsync();

        Task InitializeSteppersAsync(string selector);

        Task InitializeSelectsAsync();

        Task InitializeDatepickersAsync<T>(
            string selector,
            string inputSelector,
            DotNetObjectReference<T> objectReference,
            string callbackName, 
            MDBootstrapDatepickerOptions datepickerOptions)
            where T : class;

        Task InitializeTimepickersAsync<T>(
            string selector,
            string inputSelector,
            DotNetObjectReference<T> objectReference,
            string callbackName)
            where T : class;
        
        Task StepperStepAsync(string selector);

        Task SetAutocompleteDataAndShowAsync(string selector, IEnumerable<string> autocompleteOptions);
    }
}
