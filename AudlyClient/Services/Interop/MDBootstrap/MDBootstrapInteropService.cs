﻿using AudlyClient.Extensions.Utility;
using AudlyClient.Services.Interop.Options;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop
{
    public class MDBootstrapInteropService : IMDBootstrapInteropService
    {
        private const string INITIALIZE_STEPPER_FUNCTION = "initializeStepper";
        private const string STEPPER_STEP_FUNCTION = "stepper.step";

        private const string INITIALIZE_SELECTS_FUNCTION = "initializeSelects";

        private const string INITIALIZE_DATEPICKER_FUNCTION = "initializeDatepicker";

        private const string INITIALIZE_TIMEPICKER_FUNCTION = "initializeTimepicker";

        private const string SET_AUTOCOMPLETE_DATE_FUNCTION = "setAutocompleteDataAndShow";

        private readonly IJSRuntime _jsRuntime;

        public MDBootstrapInteropService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task InitializeSteppersAsync() =>
            await this.InitializeSteppersAsync(".stepper");

        public async Task InitializeSteppersAsync(string selector) =>
            await _jsRuntime.InvokeVoidAsync(INITIALIZE_STEPPER_FUNCTION, selector);

        public async Task InitializeSelectsAsync() =>
            await _jsRuntime.InvokeVoidAsync(INITIALIZE_SELECTS_FUNCTION, ".mdb-select");

        public async Task InitializeDatepickersAsync<T>(
            string selector, 
            string inputSelector,
            DotNetObjectReference<T> objectReference, 
            string callbackName, 
            MDBootstrapDatepickerOptions datepickerOptions)
            where T : class
        {
            await _jsRuntime.InvokeVoidAsync(
                INITIALIZE_DATEPICKER_FUNCTION, 
                selector,
                inputSelector,
                objectReference,
                callbackName,
                JsonSerializer.Serialize(datepickerOptions));
        }

        public async Task InitializeTimepickersAsync<T>(
            string selector, 
            string inputSelector, 
            DotNetObjectReference<T> objectReference, 
            string callbackName) 
            where T : class
        {
            await _jsRuntime.InvokeVoidAsync(INITIALIZE_TIMEPICKER_FUNCTION, selector, inputSelector, objectReference, callbackName);
        }

        public async Task StepperStepAsync(string selector) =>
            await _jsRuntime.InvokeVoidAsync(STEPPER_STEP_FUNCTION, selector);

        public async Task SetAutocompleteDataAndShowAsync(string selector, IEnumerable<string> autocompleteOptions) =>
            await _jsRuntime.InvokeVoidAsync(SET_AUTOCOMPLETE_DATE_FUNCTION, selector, autocompleteOptions);
    }
}
