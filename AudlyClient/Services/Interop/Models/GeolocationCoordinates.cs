﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Models
{
    public class GeolocationCoordinates
    {
        /// <summary>
        /// Gets or sets a double representing the position's latitude in decimal degrees.
        /// </summary>
        public float Latitude { get; set; }

        /// <summary>
        /// Gets or sets a double representing the position's longitude in decimal degrees.
        /// </summary>
        public float Longitude { get; set; }

        /// <summary>
        /// Gets or sets a double representing the accuracy of the latitude and longitude properties, expressed in meters.
        /// </summary>
        public double Accuracy { get; set; }
    }
}
