﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop
{
    public interface IModalInteropService
    {
        Task OpenModalAsync(string selector);

        Task OpenModalAsync(string selector, bool hideBackdrop);

        Task CloseModalAsync(string selector);
    }
}
