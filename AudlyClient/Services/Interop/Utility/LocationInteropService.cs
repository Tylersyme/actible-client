﻿using AudlyClient.Services.Interop.Models;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Utility
{
    public class LocationInteropService : ILocationInteropService
    {
        private const string GET_GEOLOCATION_COORDINATES_FUNCTION = "getGeolocationCoordinates";

        private readonly IJSRuntime _jsRuntime;

        public LocationInteropService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task<GeolocationCoordinates> GetGeolocationCoordinatesAsync<T>(
            DotNetObjectReference<T> 
            objectReference, 
            string successCallbackName,
            string errorCallbackName) where T : class =>
            await _jsRuntime.InvokeAsync<GeolocationCoordinates>(GET_GEOLOCATION_COORDINATES_FUNCTION, objectReference, successCallbackName, errorCallbackName);
    }
}
