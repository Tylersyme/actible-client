﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Utility
{
    public class JqueryUtilityInteropService : IJqueryUtilityInteropService
    {
        private const string TRIGGER_CHANGE_FUNCTION = "triggerChange";

        private readonly IJSRuntime _jsRuntime;

        public JqueryUtilityInteropService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task TriggerChangeAsync(string selector) =>
            await _jsRuntime.InvokeVoidAsync(TRIGGER_CHANGE_FUNCTION, selector);
    }
}
