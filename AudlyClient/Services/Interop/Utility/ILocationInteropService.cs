﻿using AudlyClient.Services.Interop.Models;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Utility
{
    public interface ILocationInteropService
    {
        Task<GeolocationCoordinates> GetGeolocationCoordinatesAsync<T>(
            DotNetObjectReference<T> objectReference, 
            string successCallbackName, 
            string errorCallbackName) where T : class;
    }
}
