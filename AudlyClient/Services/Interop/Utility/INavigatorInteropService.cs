﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Utility
{
    public interface INavigatorInteropService
    {
        Task<string> GetPlatformAsync();
    }
}
