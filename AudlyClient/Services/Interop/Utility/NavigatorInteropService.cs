﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Utility
{
    public class NavigatorInteropService : INavigatorInteropService
    {
        private const string GET_PLATFORM_FUNCTION = "getPlatform";

        private readonly IJSRuntime _jsRuntime;

        public NavigatorInteropService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task<string> GetPlatformAsync() =>
            await _jsRuntime.InvokeAsync<string>(GET_PLATFORM_FUNCTION);
    }
}
