﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop
{
    public class ModalInteropService : IModalInteropService
    {
        private const string OPEN_MODAL_FUNCTION = "openModal";

        private const string CLOSE_MODAL_FUNCTION = "closeModal";

        private readonly IJSRuntime _jsRuntime;

        public ModalInteropService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task OpenModalAsync(string selector) =>
            await _jsRuntime.InvokeVoidAsync(OPEN_MODAL_FUNCTION, selector);

        public async Task OpenModalAsync(string selector, bool hideBackdrop) =>
            await _jsRuntime.InvokeVoidAsync(OPEN_MODAL_FUNCTION, selector, hideBackdrop);

        public async Task CloseModalAsync(string selector) =>
            await _jsRuntime.InvokeVoidAsync(CLOSE_MODAL_FUNCTION, selector);
    }
}
