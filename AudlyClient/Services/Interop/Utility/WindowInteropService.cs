﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Utility
{
    public class WindowInteropService : IWindowInteropService
    {
        private const string OPEN_WINDOW_FUNCTION = "open";

        private readonly IJSRuntime _jsRuntime;

        public WindowInteropService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        public async Task OpenWindowAsync(string url) =>
            await _jsRuntime.InvokeVoidAsync(OPEN_WINDOW_FUNCTION, url);
    }
}
