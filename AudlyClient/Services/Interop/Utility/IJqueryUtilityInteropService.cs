﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Utility
{
    public interface IJqueryUtilityInteropService
    {
        Task TriggerChangeAsync(string selector);
    }
}
