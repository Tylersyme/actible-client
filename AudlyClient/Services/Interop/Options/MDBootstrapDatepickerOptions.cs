﻿using AudlyClient.Services.Converters.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AudlyClient.Services.Interop.Options
{
    public struct MDBootstrapDatepickerOptions
    {
        [JsonConverter(typeof(JavascriptDateTimeConverter))]
        [JsonPropertyName("min")]
        public DateTime MinimumDate { get; set; }

        [JsonPropertyName("format")]
        public string DateFormat { get; set; }

        [JsonPropertyName("formatSubmit")]
        public string DateFormatSubmit { get; set; }

        public bool ShowMonthsFull { get; set; }
    }
}
