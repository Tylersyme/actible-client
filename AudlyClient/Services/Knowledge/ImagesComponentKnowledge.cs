﻿using AudlyClient.Models;
using AudlyClient.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Knowledge
{
    public class ImagesComponentKnowledge : IImagesComponentKnowlege
    {
        public string GetPrimaryImageUrl(ImagesComponent imagesComponent, string placeholderImageUrl)
        {
            if (imagesComponent == null || !imagesComponent.ImageUrls.Any())
                return placeholderImageUrl;

            return imagesComponent.ImageUrls.First();
        }
    }
}
