﻿using AudlyClient.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Knowledge
{
    public interface IImagesComponentKnowlege
    {
        string GetPrimaryImageUrl(ImagesComponent imagesComponent, string placeholderImageUrl);
    }
}
