﻿using AudlyClient.Models;
using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services
{
    public class ActivityNavigationService : IActivityNavigationService
    {
        private readonly INavigationService _navigationService;

        public ActivityNavigationService(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void NavigateToPlanActivity(Activity activity) =>
            _navigationService.NavigateTo(string.Format(Routes.PLAN_ACTIVITY, activity.Source.Identifier));
    }
}
