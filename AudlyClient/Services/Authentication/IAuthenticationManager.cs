﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Authentication
{
    public interface IAuthenticationManager
    {
        void StartLogin();

        void StartLogout();
    }
}
