﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Authentication
{
    public interface IAuthTokenService
    {
        Task<string> GetAuthTokenAsync();

        Task SetLoginCredentialsAsync(AuthenticatedUser authenticatedUser);

        Task<bool> AuthTokenExistsAsync();

        Task<bool> IsLoggedInAsync();
    }
}
