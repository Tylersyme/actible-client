﻿using JWT.Algorithms;
using JWT.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Authentication
{
    public class JwtService : IJwtService
    {
        private const string EXPIRATION_CLAIM_NAME = "exp";

        public DateTime GetExpiration(string rawAuthToken)
        {
            var payload = new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .Decode<IDictionary<string, object>>(rawAuthToken);

            var expirationSeconds = (long)payload[EXPIRATION_CLAIM_NAME];

            var expiration = DateTimeOffset.FromUnixTimeSeconds(expirationSeconds).UtcDateTime;

            return expiration;
        }
    }
}
