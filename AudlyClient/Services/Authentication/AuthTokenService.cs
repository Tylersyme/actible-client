﻿using AudlyClient.Models;
using Blazored.LocalStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Authentication
{
    public class AuthTokenService : IAuthTokenService
    {
        private const string AUTH_TOKEN_KEY = "auth_token";

        private readonly ILocalStorageService _localStorageService;

        private readonly IJwtService _jwtService;

        public AuthTokenService(
            ILocalStorageService localStorageService,
            IJwtService jwtService)
        {
            _localStorageService = localStorageService;
            _jwtService = jwtService;
        }

        public async Task<string> GetAuthTokenAsync() =>
            await _localStorageService.GetItemAsStringAsync(AUTH_TOKEN_KEY);

        public async Task SetLoginCredentialsAsync(AuthenticatedUser authenticatedUser) =>
            await _localStorageService.SetItemAsync(AUTH_TOKEN_KEY, authenticatedUser.Token);

        public async Task<bool> AuthTokenExistsAsync() =>
            await _localStorageService.ContainKeyAsync(AUTH_TOKEN_KEY);

        /// <summary>
        /// Returns whether the user is currently logged in.
        /// </summary>
        /// <returns>True if user's login has not expired, False otherwise</returns>
        public async Task<bool> IsLoggedInAsync()
        {
            if (!await this.AuthTokenExistsAsync())
                return false;

            var authToken = await this.GetAuthTokenAsync();

            var expiration = _jwtService.GetExpiration(authToken);

            return DateTime.Now < expiration;
        }
    }
}
