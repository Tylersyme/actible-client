﻿using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using Flurl;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Authentication
{
    public class AuthenticationManager : IAuthenticationManager
    {
        private readonly INavigationService _navigationService;

        private readonly SignOutSessionStateManager _signOutSessionStateManager;

        public AuthenticationManager(
            INavigationService navigationService,
            SignOutSessionStateManager signOutSessionStateManager)
        {
            _navigationService = navigationService;
            _signOutSessionStateManager = signOutSessionStateManager;
        }

        public void StartLogin()
        {
            var navigationUrl = this.GetAuthenticationUrl(Routes.SIGNUP_SIGNIN);

            _navigationService.NavigateTo(navigationUrl);
        }

        public void StartLogout()
        {
            var navigationUrl = this.GetAuthenticationUrl(Routes.LOGOUT);

            _signOutSessionStateManager.SetSignOutState();

            _navigationService.NavigateTo(navigationUrl);
        }

        private Url GetAuthenticationUrl(string authenticationRoute) =>
            authenticationRoute.SetQueryParam("returnUrl", _navigationService.Uri);
    }
}
