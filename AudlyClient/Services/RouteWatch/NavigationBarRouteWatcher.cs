﻿using AudlyClient.ComponentData.Context;
using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using Easy.MessageHub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.RouteWatch
{
    public class NavigationBarRouteWatcher : RouteWatcher
    {
        public bool IsFullscreen { get; private set; }

        private NavigationBarContext _navigationBarContext;

        public NavigationBarRouteWatcher(
            IMessageHub messageHub,
            IRouteManager routeManager,
            NavigationBarContext navigationBarContext)
            : base(messageHub, routeManager)
        {
            _navigationBarContext = navigationBarContext;
        }

        protected override void PreNavigated(RouteDefinition routeDefinition, RouteDefinition previousRouteDefinition)
        {
            if (!routeDefinition.ContainsRouteDataKey(RouteDataKeys.NAVIGATION_BAR))
                throw new NullReferenceException("Route must have navigation bar data.");

            var navigationBarRouteData = routeDefinition.GetRouteData<NavigationBarRouteData>(RouteDataKeys.NAVIGATION_BAR);
            var previousNavigationBarRouteData = routeDefinition.GetRouteData<NavigationBarRouteData>(RouteDataKeys.NAVIGATION_BAR);

            // Navigation bar
            _navigationBarContext.ShowNavigationBar = !navigationBarRouteData.IsFullscreen;
            _navigationBarContext.HideNavigationArrow = navigationBarRouteData.HideNavigationArrow;
            _navigationBarContext.OverrideNavigationArrowClick = false;
            _navigationBarContext.Title = navigationBarRouteData.Title;

            if (ShouldRemoveRightSideActionButton())
            {
                _navigationBarContext.RightSideActionButtonText = null;
                _navigationBarContext.OnRightSideActionButtonClicked = null;
            }

            // Bottom tabs
            _navigationBarContext.ShowBottomTabs = !navigationBarRouteData.IsFullscreen && !navigationBarRouteData.HideBottomTabs;

            bool ShouldRemoveRightSideActionButton() =>
                !navigationBarRouteData.UsesRightSideActionButton ||
                (previousNavigationBarRouteData != null && routeDefinition != previousRouteDefinition);
        }
    }
}
