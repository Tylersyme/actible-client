﻿using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using AudlyClient.Navigation.Services.EventArgs;
using Easy.MessageHub;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.RouteWatch
{
    public abstract class RouteWatcher
    {
        private readonly IMessageHub _messageHub;

        private readonly IRouteManager _routeManager;

        public RouteWatcher(
            IMessageHub messageHub,
            IRouteManager routeManager)
        {
            _messageHub = messageHub;
            _routeManager = routeManager;

            this.Subscribe();
        }

        protected abstract void PreNavigated(RouteDefinition routeDefinition, RouteDefinition previousRouteDefinition);

        private void OnNavigation(PreNavigationEventArgs eventArgs)
        {
            var routeDefinition = _routeManager.GetMatchingRouteDefinition(eventArgs.Location);

            if (routeDefinition == null)
                return;

            var previousRouteDefinition = _routeManager.GetMatchingRouteDefinition(eventArgs.PreviousLocation);

            this.PreNavigated(routeDefinition, previousRouteDefinition);
        }

        private void Subscribe()
        {
            _messageHub.Subscribe<PreNavigationEventArgs>(OnNavigation);
        }
    }
}
