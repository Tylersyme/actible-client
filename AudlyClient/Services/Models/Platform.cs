﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Models
{
    public class Platform
    {
        public static readonly IEnumerable<Platform> ALL = new List<Platform>();

        public static readonly Platform IPhone = new Platform(PlatformGroups.APPLE, "iPhone");
        public static readonly Platform IPad = new Platform(PlatformGroups.APPLE, "iPad");
        public static readonly Platform IPod = new Platform(PlatformGroups.APPLE, "iPod");

        public string PlatformGroup { get; private set; }

        public string WebPlatformName { get; private set; }

        private Platform(string platformGroup, string webPlatformName)
        {
            this.PlatformGroup = platformGroup;

            this.WebPlatformName = webPlatformName;

            ((List<Platform>)ALL).Add(this);
        }

        public static Platform GetByWebPlatformName(string webPlatformName) =>
            ALL.FirstOrDefault(p => p.WebPlatformName == webPlatformName) ?? new Platform(PlatformGroups.UNKNOWN, webPlatformName);

        public static IEnumerable<Platform> GetPlatformsOfGroup(string platformGroup) =>
            ALL.Where(p => p.PlatformGroup == platformGroup);
    }
}
