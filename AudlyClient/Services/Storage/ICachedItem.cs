﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Storage
{
    public interface ICachedItem
    {
        DateTime ExpirationDate { get; }
    }
}
