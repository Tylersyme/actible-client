﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Storage
{
    public class CachedItem<T> : ICachedItem
    {
        public T Item { get; set; }

        public DateTime ExpirationDate { get; set; }
    }
}
