﻿using AudlyClient.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Clients.Builders
{
    public class PlacesAutocompleteRequestBuilder : IPlacesAutocompleteRequestBuilder
    {
        private PlaceAutocompleteRequest _placeAutocompleteRequest;

        public IPlacesAutocompleteRequestBuilder CreateAsNewSession()
        {
            _placeAutocompleteRequest = new PlaceAutocompleteRequest
            {
                SessionToken = Guid.NewGuid().ToString(),
            };

            return this;
        }

        public IPlacesAutocompleteRequestBuilder ContinueAsExistingSession(PlaceAutocompleteRequest previousRequest, string input)
        {
            _placeAutocompleteRequest = previousRequest;

            this.WithInput(input);

            return this;
        }

        public IPlacesAutocompleteRequestBuilder WithInput(string input)
        {
            _placeAutocompleteRequest.Input = input;

            return this;
        }

        public PlaceAutocompleteRequest Build()
        {
            if (string.IsNullOrEmpty(_placeAutocompleteRequest.Input))
                throw new NullReferenceException();

            return _placeAutocompleteRequest;
        }
    }
}
