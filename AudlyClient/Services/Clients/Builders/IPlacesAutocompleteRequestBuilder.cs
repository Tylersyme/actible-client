﻿using AudlyClient.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Clients.Builders
{
    public interface IPlacesAutocompleteRequestBuilder
    {
        IPlacesAutocompleteRequestBuilder CreateAsNewSession();

        IPlacesAutocompleteRequestBuilder ContinueAsExistingSession(PlaceAutocompleteRequest previousRequest, string input);

        IPlacesAutocompleteRequestBuilder WithInput(string input);

        PlaceAutocompleteRequest Build();
    }
}
