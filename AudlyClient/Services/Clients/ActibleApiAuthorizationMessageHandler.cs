﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Clients
{
    public class ActibleApiAuthorizationMessageHandler : AuthorizationMessageHandler
    {
        public ActibleApiAuthorizationMessageHandler(
            IAccessTokenProvider provider, 
            NavigationManager navigation) 
            : base(provider, navigation)
        {
            ConfigureHandler(
                new[] { Constants.API_BASE_ADDRESS });
        }
    }
}
