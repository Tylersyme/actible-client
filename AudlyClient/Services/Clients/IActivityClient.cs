﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudlyClient.Models;
using AudlyClient.Models.Components;
using AudlyClient.Requests;

namespace AudlyClient.Services.Clients
{
    public interface IActivityClient
    {
        Task<Activity> GetRandomActivityAsync();

        Task<List<Activity>> GetNextActivitiesAsync(ActivityRequest activityRequest);

        Task<List<Component>> GetActivityDetailsAsync(ActivityDetailsRequest activityDetailsRequest);

        Task<List<SavedActivity>> GetSavedActivitiesAsync(SavedActivitiesRequest request);

        Task<Guid> ScheduleActivityAsync(ScheduleActivityRequest request);

        Task SaveActivityAsync(string activityIdentifier);

        Task DeleteSavedActivityAsync(string activityIdentifier);

        Task UpdateScheduledActivityAsync(UpdateScheduledActivityRequest request);
    }
}
