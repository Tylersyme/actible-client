﻿using AudlyClient.Exceptions;
using AudlyClient.Extensions.Utility;
using AudlyClient.Models;
using AudlyClient.Models.Components;
using AudlyClient.Requests;
using AudlyClient.Responses;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace AudlyClient.Services.Clients
{
    public class ActivityClient : IActivityClient
    {
        private const string BASE_ENDPOINT = "api/activity";

        private const string GET_NEXT_ACTIVITIES_BY_FILTER_ACTION = "GetNextActivities";

        private const string GET_ACTIVITY_DETAILS_ACTION = "GetActivityDetails";

        private const string GET_SAVED_ACTIVITIES_ACTION = "GetSavedActivities";

        private const string SAVE_ACTIVITY_ACTION = "SaveActivity";

        private const string SCHEDULE_ACTIVITY_ACTION = "ScheduleActivity";

        private const string DELETE_SAVED_ACTIVITY_ACTION = "DeleteSavedActivity";

        private const string UPDATE_SCHEDULED_ACTIVITY_ACTION = "UpdateScheduledActivity";

        private readonly HttpClient _httpClient;

        public ActivityClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Activity> GetRandomActivityAsync()
        {
            var response = await _httpClient.GetAsync(BASE_ENDPOINT);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<Activity>();

            return content;
        }

        public async Task<List<Activity>> GetNextActivitiesAsync(ActivityRequest request)
        {
            var requestUrl = this.GetNextActivitiesByFilterUrl(request);

            var response = await _httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var json = await response.Content.ReadAsStringAsync();

            var content = await response.Content.ReadFromJsonAsync<List<ActivityResponse>>();

            return content
                .Select(a => a.Activity)
                .ToList();
        }

        public async Task<List<Component>> GetActivityDetailsAsync(ActivityDetailsRequest request)
        {
            var requestUrl = this.GetActivityDetailsUrl(request);

            var response = await _httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<ActivityDetailsResponse>();

            return content.DetailComponents;
        }

        public async Task<List<SavedActivity>> GetSavedActivitiesAsync(SavedActivitiesRequest request)
        {
            var requestUrl = this.GetSavedActivitiesUrl(request);

            var response = await _httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<SavedActivitiesResponse>();

            var savedActivities = content.SavedActivities
                .SelectMany(s => s.SavedActivityDetails.Select(sad => new SavedActivity
                {
                    Activity = s.Activity,
                    SavedActivityDetails = sad,
                }))
                .ToList();

            return savedActivities;
        }

        public async Task SaveActivityAsync(string activityIdentifier)
        {
            var requestUrl = this.GetSaveActivityUrl(activityIdentifier);

            var response = await _httpClient.PostAsync(requestUrl, null);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();
        }

        public async Task<Guid> ScheduleActivityAsync(ScheduleActivityRequest request)
        {
            var requestUrl = this.GetScheduleActivityUrl();

            var requestJson = JsonConvert.SerializeObject(request);

            var response = await _httpClient.PostAsync(requestUrl, requestJson.ToJsonContent());

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<ScheduleActivityResponse>();

            return content.SavedActivityDetailsId;
        }

        public async Task DeleteSavedActivityAsync(string activityIdentifier)
        {
            var requestUrl = this.GetDeleteSavedActivityUrl(activityIdentifier);

            var response = await _httpClient.DeleteAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();
        }

        public async Task UpdateScheduledActivityAsync(UpdateScheduledActivityRequest request)
        {
            var requestUrl = this.GetUpdateScheduledActivityUrl();

            var requestJson = JsonConvert.SerializeObject(request);

            var response = await _httpClient.PutAsync(requestUrl, requestJson.ToJsonContent());

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();
        }

        private string GetNextActivitiesByFilterUrl(ActivityRequest request)
        {
            var actionUri = Path.Combine(BASE_ENDPOINT, GET_NEXT_ACTIVITIES_BY_FILTER_ACTION);

            return QueryHelpers.AddQueryString(actionUri, "requestJson", JsonConvert.SerializeObject(request));
        }

        private string GetActivityDetailsUrl(ActivityDetailsRequest request)
        {
            var actionUri = Path.Combine(BASE_ENDPOINT, GET_ACTIVITY_DETAILS_ACTION);

            return QueryHelpers.AddQueryString(actionUri, "requestJson", JsonConvert.SerializeObject(request));
        }

        private string GetSavedActivitiesUrl(SavedActivitiesRequest request)
        {
            var actionUri = Path.Combine(BASE_ENDPOINT, GET_SAVED_ACTIVITIES_ACTION);

            return QueryHelpers.AddQueryString(actionUri, "requestJson", JsonConvert.SerializeObject(request));
        }

        private string GetSaveActivityUrl(string activityIdentifier)
        {
            var actionUri = Path.Combine(BASE_ENDPOINT, SAVE_ACTIVITY_ACTION);

            return QueryHelpers.AddQueryString(actionUri, "activityIdentifier", activityIdentifier);
        }

        private string GetDeleteSavedActivityUrl(string activityIdentifier)
        {
            var actionUri = Path.Combine(BASE_ENDPOINT, DELETE_SAVED_ACTIVITY_ACTION);

            return QueryHelpers.AddQueryString(actionUri, "activityIdentifier", activityIdentifier);
        }

        private string GetScheduleActivityUrl() =>
            Path.Combine(BASE_ENDPOINT, SCHEDULE_ACTIVITY_ACTION);

        private string GetUpdateScheduledActivityUrl() =>
            Path.Combine(BASE_ENDPOINT, UPDATE_SCHEDULED_ACTIVITY_ACTION);
    }
}
