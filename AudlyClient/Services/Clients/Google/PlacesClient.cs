﻿using AudlyClient.Exceptions;
using AudlyClient.Requests;
using AudlyClient.Responses;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Flurl;
using System.Text.Json;
using System.Net.Http;

namespace AudlyClient.Services.Clients.Google
{
    public class PlacesClient : IPlacesClient
    {
        private const string BASE_ENDPOINT = "api/place";

        private const string GET_AUTOCOMPLETE_PREDICTIONS_ACTION = "GetAutocompletePredictions";

        private const string GET_AUTOCOMPLETE_DETAILS_ACTION = "GetAutocompleteDetails";

        private readonly HttpClient _httpClient;

        public PlacesClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<PlaceAutocompleteResponse>> GetAutocompletePredictionsAsync(PlaceAutocompleteRequest request)
        {
            var requestUrl = this.GetAutocompletePredictionsUrl(request);

            var response = await _httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<IEnumerable<PlaceAutocompleteResponse>>();

            return content;
        }

        public async Task<PlaceAutocompleteDetailsResponse> GetAutocompleteDetailsAsync(PlaceAutocompleteDetailsRequest request)
        {
            var requestUrl = this.GetAutocompleteDetailsUrl(request);

            var response = await _httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<PlaceAutocompleteDetailsResponse>();

            return content;
        }

        private string GetAutocompletePredictionsUrl(PlaceAutocompleteRequest request) =>
            BASE_ENDPOINT
                .AppendPathSegment(GET_AUTOCOMPLETE_PREDICTIONS_ACTION)
                .SetQueryParam("requestJson", JsonSerializer.Serialize(request));

        private string GetAutocompleteDetailsUrl(PlaceAutocompleteDetailsRequest request) =>
            BASE_ENDPOINT
                .AppendPathSegment(GET_AUTOCOMPLETE_DETAILS_ACTION)
                .SetQueryParam("requestJson", JsonSerializer.Serialize(request));
    }
}
