﻿using AudlyClient.Requests;
using AudlyClient.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.Clients.Google
{
    public interface IPlacesClient
    {
        Task<IEnumerable<PlaceAutocompleteResponse>> GetAutocompletePredictionsAsync(PlaceAutocompleteRequest request);

        Task<PlaceAutocompleteDetailsResponse> GetAutocompleteDetailsAsync(PlaceAutocompleteDetailsRequest request);
    }
}
