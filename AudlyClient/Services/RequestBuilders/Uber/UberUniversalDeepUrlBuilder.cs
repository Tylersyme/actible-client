﻿using AudlyClient.AppSetting;
using AudlyClient.Services.RequestBuilders.Uber.Urls;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Flurl;
using AudlyClient.Services.Utility;
using AudlyClient.Services.Interop.Models;

namespace AudlyClient.Services.RequestBuilders.Uber
{
    public class UberUniversalDeepUrlBuilder : IUberUniversalDeepUrlBuilder
    {
        private const string BASE_ENDPOINT = "https://m.uber.com/ul/";

        private UberUniversalDeepUrl _uberUniversalDeepUrl;

        private readonly UberSettings _uberSettings;

        public UberUniversalDeepUrlBuilder(UberSettings uberSettings)
        {
            _uberSettings = uberSettings;
        }

        public IUberUniversalDeepUrlBuilder Create(string pickupNickname, string dropoffNickname)
        {
            if (string.IsNullOrEmpty(pickupNickname))
                throw new ArgumentException(pickupNickname);

            if (string.IsNullOrEmpty(dropoffNickname))
                throw new ArgumentException(dropoffNickname);

            _uberUniversalDeepUrl = new UberUniversalDeepUrl
            {
                ClientId = _uberSettings.ClientId,
                Action = UberActions.PICKUP,
                PickupNickname = pickupNickname,
                DropoffNickname = dropoffNickname,
            };

            return this;
        }

        public IUberUniversalDeepUrlBuilder WithPickupCoordinates(GeolocationCoordinates geolocationCoordinates)
        {
            _uberUniversalDeepUrl.PickupLatitude = geolocationCoordinates.Latitude;

            _uberUniversalDeepUrl.PickupLongitude = geolocationCoordinates.Longitude;

            return this;
        }

        public IUberUniversalDeepUrlBuilder WithDropoffCoordinates(GeolocationCoordinates geolocationCoordinates)
        {
            _uberUniversalDeepUrl.DropoffLatitude = geolocationCoordinates.Latitude;

            _uberUniversalDeepUrl.DropoffLongitude = geolocationCoordinates.Longitude;

            return this;
        }

        public string Build()
        {
            var url = BASE_ENDPOINT
                .SetQueryParam("client_id", _uberUniversalDeepUrl.ClientId)
                .SetQueryParam("action", _uberUniversalDeepUrl.Action)
                .SetQueryParam("pickup[nickname]", _uberUniversalDeepUrl.PickupNickname)
                .SetQueryParam("dropoff[latitude]", _uberUniversalDeepUrl.DropoffLatitude)
                .SetQueryParam("dropoff[longitude]", _uberUniversalDeepUrl.DropoffLongitude)
                .SetQueryParam("dropoff[nickname]", _uberUniversalDeepUrl.DropoffNickname);

            if (_uberUniversalDeepUrl.PickupLatitude == null || _uberUniversalDeepUrl.PickupLongitude == null)
                url.SetQueryParam("pickup", "my_location");
            else
                url
                    .SetQueryParam("pickup[longitude]", _uberUniversalDeepUrl.PickupLongitude)
                    .SetQueryParam("pickup[nickname]", _uberUniversalDeepUrl.PickupNickname);

            return url;
        }
    }
}
