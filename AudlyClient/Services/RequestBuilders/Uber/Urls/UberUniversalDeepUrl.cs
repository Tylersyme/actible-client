﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.RequestBuilders.Uber.Urls
{
    internal class UberUniversalDeepUrl
    {
        public string ClientId { get; set; }

        public string Action { get; set; }

        public string PickupNickname { get; set; }

        public string DropoffNickname { get; set; }

        public float? PickupLatitude { get; set; }

        public float? PickupLongitude { get; set; }

        public float? DropoffLatitude { get; set; }

        public float? DropoffLongitude { get; set; }
    }
}
