﻿using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.RequestBuilders.Uber
{
    public interface IUberUniversalDeepUrlBuilder
    {
        IUberUniversalDeepUrlBuilder Create(string pickupNickname, string dropoffNickname);

        IUberUniversalDeepUrlBuilder WithPickupCoordinates(GeolocationCoordinates geolocationCoordinates);

        IUberUniversalDeepUrlBuilder WithDropoffCoordinates(GeolocationCoordinates geolocationCoordinates);

        string Build();
    }
}
