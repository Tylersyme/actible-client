﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services.RequestBuilders.Uber
{
    public static class UberActions
    {
        public const string PICKUP = "setPickup";

        public const string APPLY_PROMO = "applyPromo";
    }
}
