﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services
{
    public interface ISavedActivityService
    {
        Task ScheduleActivityAsync(Activity activity, SavedActivityDetails savedActivityDetails);

        Task SaveActivityForLaterAsync(Activity activity);

        Task UpdateSavedActivityAsync(SavedActivity savedActivity);

        Task<List<SavedActivity>> GetSavedActivitiesAsync();

        Task DeleteSavedActivityAsync(string activityIdentifier);
    }
}
