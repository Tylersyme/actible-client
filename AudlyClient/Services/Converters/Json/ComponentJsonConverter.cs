﻿using AudlyClient.Enums;
using AudlyClient.Extensions.Utility;
using AudlyClient.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AudlyClient.Services.Converters.Json
{
    public class ComponentJsonConverter : JsonConverter<Component>
    {
        public override Component Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            using (var jsonDocument = JsonDocument.ParseValue(ref reader))
            {
                var rawJson = jsonDocument.RootElement.GetRawText();

                var componentTypeElement = jsonDocument.RootElement.GetProperty(nameof(Component.Type).ToLower());

                var componentType = (ComponentType)componentTypeElement.GetInt32();

                switch (componentType)
                {
                    case ComponentType.PlaceInfo:
                        return JsonSerializer.Deserialize<PlaceInfoComponent>(rawJson, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                    case ComponentType.LazyPlaceReviews:
                        return JsonSerializer.Deserialize<LazyPlaceReviewsComponent>(rawJson, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                    case ComponentType.PlaceReviews:
                        return JsonSerializer.Deserialize<PlaceReviewsComponent>(rawJson, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                    case ComponentType.Images:
                        return JsonSerializer.Deserialize<ImagesComponent>(rawJson, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                    case ComponentType.Location:
                        return JsonSerializer.Deserialize<LocationComponent>(rawJson, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public override void Write(Utf8JsonWriter writer, Component value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
