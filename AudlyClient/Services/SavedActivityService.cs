﻿using AudlyClient.Extensions;
using AudlyClient.Models;
using AudlyClient.Requests;
using AudlyClient.Services.Clients;
using AudlyClient.Services.Storage;
using Blazored.LocalStorage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace AudlyClient.Services
{
    public class SavedActivityService : ISavedActivityService
    {
        private const string SAVED_ACTIVITIES_KEY = "saved_activities";

        private readonly IActivityClient _activityClient;
        private readonly ILocalStorageService _localStorageService;

        public SavedActivityService(
            IActivityClient activityClient,
            ILocalStorageService localStorageService)
        {
            _activityClient = activityClient;
            _localStorageService = localStorageService;
        }

        public async Task ScheduleActivityAsync(Activity activity, SavedActivityDetails savedActivityDetails)
        {
            var cacheDuration = this.GetMaxActivityCacheDuration(activity);

            var id = await _activityClient.ScheduleActivityAsync(new ScheduleActivityRequest
            {
                ActivityIdentifier = activity.Source.Identifier,
                SavedActivityDetails = savedActivityDetails,
            });

            savedActivityDetails.Id = id;

            await this.CacheSavedActivityAsync(new SavedActivity
            {
                Activity = activity,
                SavedActivityDetails = savedActivityDetails,
            },
            cacheDuration);
        }

        public async Task SaveActivityForLaterAsync(Activity activity)
        {
            var cacheDuration = this.GetMaxActivityCacheDuration(activity);

            await this.CacheSavedActivityAsync(new SavedActivity
            {
                Activity = activity,
            }, 
            cacheDuration);

            await _activityClient.SaveActivityAsync(activity.Source.Identifier);
        }

        public async Task UpdateSavedActivityAsync(SavedActivity savedActivity)
        {
            await _activityClient.UpdateScheduledActivityAsync(new UpdateScheduledActivityRequest
            {
                SavedActivityDetails = savedActivity.SavedActivityDetails,
            });

            var cachedActivities = (await this.GetUnexpiredCachedActivitiesAsync()).ToList();

            var existingCachedActivity = cachedActivities.First(ca => ca.Item.Activity.Source.Identifier == savedActivity.Activity.Source.Identifier);

            existingCachedActivity.Item = savedActivity;

            await this.OverwriteCachedItemsAsync(cachedActivities);
        }

        public async Task<List<SavedActivity>> GetSavedActivitiesAsync()
        {
            var savedActivities = (await this.GetUnexpiredCachedActivitiesAsync())
                .Select(ca => ca.Item)
                .ToList();

            var cachedActivityIdentifiers = savedActivities.Select(a => a.Activity.Source.Identifier);

            // Get all activities which weren't cached from the api
            var uncachedActivities = (await _activityClient
                .GetSavedActivitiesAsync(new SavedActivitiesRequest
                {
                    ExcludedActivitySourceIdentifiers = cachedActivityIdentifiers,
                }))
                .Where(a => !a.IsFinished());

            // Cache all previously uncached activities from the api
            await this.CacheActivitiesAsync(uncachedActivities.Select(a => (a, this.GetMaxActivityCacheDuration(a.Activity))));

            savedActivities.AddRange(uncachedActivities);

            return savedActivities;
        }

        public async Task DeleteSavedActivityAsync(string activityIdentifier)
        {
            var remainingCachedActivities = (await this.GetUnexpiredCachedActivitiesAsync())
                 .Where(ca => ca.Item.Activity.Source.Identifier != activityIdentifier)
                 .ToList();

            await this.OverwriteCachedItemsAsync(remainingCachedActivities);

            await _activityClient.DeleteSavedActivityAsync(activityIdentifier);
        }

        private async Task CacheActivitiesAsync(IEnumerable<(SavedActivity SavedActivity, TimeSpan TimeSpan)> activitiesToCache)
        {
            var cachedActivites = await this.GetUnexpiredCachedActivitiesAsync();

            foreach (var activityToCache in activitiesToCache)
                cachedActivites.Add(new CachedItem<SavedActivity>
                {
                    Item = activityToCache.SavedActivity,
                    ExpirationDate = DateTime.Now.Add(activityToCache.TimeSpan),
                });

            await this.OverwriteCachedItemsAsync(cachedActivites);
        }

        private async Task CacheActivitiesAsync(IEnumerable<SavedActivity> activities, TimeSpan cacheDuration) =>
            await this.CacheActivitiesAsync(activities.Select(a => (a, cacheDuration)));

        private async Task CacheSavedActivityAsync(SavedActivity activity, TimeSpan cacheDuration) =>
            await this.CacheActivitiesAsync(new SavedActivity[] { activity }, cacheDuration);

        private async Task OverwriteCachedItemsAsync(IEnumerable<CachedItem<SavedActivity>> cachedItems)
        {
            var json = JsonConvert.SerializeObject(cachedItems, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
            });

            await _localStorageService.SetItemAsync(SAVED_ACTIVITIES_KEY, json);
        }

        private async Task<List<CachedItem<SavedActivity>>> GetUnexpiredCachedActivitiesAsync()
        {
            var cachedActivies = await this.GetCachedActivitiesAsync();

            return cachedActivies
                .Where(ca => !ca.IsExpired())
                .ToList();
        }

        private async Task<List<CachedItem<SavedActivity>>> GetCachedActivitiesAsync()
        {
            if (!await _localStorageService.ContainKeyAsync(SAVED_ACTIVITIES_KEY))
                return new List<CachedItem<SavedActivity>>();

            var cachedActivitiesJson = await _localStorageService.GetItemAsStringAsync(SAVED_ACTIVITIES_KEY);

            var cachedActivities = JsonConvert.DeserializeObject<List<CachedItem<SavedActivity>>>(cachedActivitiesJson, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All,
            });

            return cachedActivities;
        }

        private TimeSpan GetMaxActivityCacheDuration(Activity activity) =>
            TimeSpan.FromMilliseconds(activity.Source.MaxCacheDurationMilliseconds);
    }
}
