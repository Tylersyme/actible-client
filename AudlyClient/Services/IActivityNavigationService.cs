﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Services
{
    public interface IActivityNavigationService
    {
        void NavigateToPlanActivity(Activity activity);
    }
}
