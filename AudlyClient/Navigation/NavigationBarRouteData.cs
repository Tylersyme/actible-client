﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation
{
    public class NavigationBarRouteData
    {
        public bool IsFullscreen { get; set; }

        public bool HideBottomTabs { get; set; }

        public bool HideNavigationArrow { get; set; }

        public bool UsesRightSideActionButton { get; set; }

        public string Title { get; set; }
    }
}
