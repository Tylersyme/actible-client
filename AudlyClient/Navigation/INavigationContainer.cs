﻿using AudlyClient.Navigation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation
{
    public interface INavigationContainer : INavigationHistoryStore
    {
        string UniqueName { get; }

        INavigationContainer Parent { get; set; }

        INavigationContainer WithRouteRule(string routeRule);

        INavigationContainer WithChildNavigationContainer(INavigationContainer child);

        bool TryGetNavigationContainerContainingRoute(string route, out INavigationContainer navigationContainer);

        INavigationContainer GetByName(string name);
    }
}
