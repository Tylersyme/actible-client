﻿using AudlyClient.Extensions.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tavis.UriTemplates;

namespace AudlyClient.Navigation
{
    public class RouteDefinition
    {
        public UriTemplate UriTemplate { get; private set; }

        private IDictionary<string, object> _routeData = new Dictionary<string, object>();

        public RouteDefinition(string uriTemplate)
        {
            this.UriTemplate = new UriTemplate(uriTemplate);
        }

        public RouteDefinition WithRouteData<T>(string key, T data)
        {
            _routeData.Add(key, data);

            return this;
        }

        public bool ContainsRouteDataKey(string key) =>
            _routeData.ContainsKey(key);

        public T GetRouteData<T>(string key) =>
            (T)_routeData[key];

        public bool Matches(string url) =>
            url != null && this.UriTemplate.Matches(url);
    }
}
