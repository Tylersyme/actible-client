﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation
{
    public static class RouteContainerNames
    {
        public const string HOME = "home";

        public const string ACTIVITIES = "activities";
    }
}
