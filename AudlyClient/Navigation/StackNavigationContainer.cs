﻿using AudlyClient.Navigation.Services;
using AudlyClient.Navigation.Services.EventArgs;
using Easy.MessageHub;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation
{
    public class StackNavigationContainer : NavigationHistoryStore, INavigationContainer
    {
        public INavigationContainer Parent { get; set; }

        private List<INavigationContainer> _children = new List<INavigationContainer>();
        public IReadOnlyList<INavigationContainer> Children =>
            _children;

        public string UniqueName { get; private set; }

        private List<string> _routeRules = new List<string>();

        public StackNavigationContainer(string uniqueName, string startingRoute)
            : base(startingRoute)
        {
            this.UniqueName = uniqueName;
        }

        public INavigationContainer WithRouteRule(string routeRule)
        {
            _routeRules.Add(routeRule);

            return this;
        }

        public INavigationContainer WithChildNavigationContainer(INavigationContainer child)
        {
            child.Parent = this;

            _children.Add(child);

            return this;
        }

        public bool TryGetNavigationContainerContainingRoute(string route, out INavigationContainer navigationContainer)
        {
            navigationContainer = default;

            var path = !string.IsNullOrEmpty(route) && route != "/"
                ? new Uri(route).LocalPath
                : route;

            var pathParts = this.GetPathParts(path);

            var unmatchedPathParts = this.GetUnmatchedPathParts(pathParts);

            // No path parts were matched
            if (pathParts.Length == unmatchedPathParts.Length && _routeRules.First() != "/")
                return false;

            if (unmatchedPathParts.Length == 0)
            {
                navigationContainer = this;

                return true;
            }

            var unmatchedPath = $"{Path.DirectorySeparatorChar}{Path.Combine(unmatchedPathParts)}";

            var childNavigationContainer = default(INavigationContainer);

            // Check if any children match the remaining umatched path
            var anyChildMatches = this.Children.Any(n => n.TryGetNavigationContainerContainingRoute(unmatchedPath, out childNavigationContainer));

            if (anyChildMatches)
                navigationContainer = childNavigationContainer;
            else
                navigationContainer = this;

            return true;
        }

        public INavigationContainer GetByName(string name)
        {
            if (this.UniqueName == name)
                return this;

            var childWithName = this.Children.FirstOrDefault(c => c.GetByName(name) != null);

            return childWithName;
        }

        private string[] GetUnmatchedPathParts(string[] pathParts)
        {
            var totalPathPartsToRemove = 0;

            if (_routeRules.First() == "/")
                return pathParts;

            foreach (var pathPart in pathParts)
            {
                var matchesRule = this.PathPartMatchesAnyRule(pathPart);

                if (!matchesRule)
                    break;

                ++totalPathPartsToRemove;
            }

            return pathParts
                .Skip(totalPathPartsToRemove)
                .ToArray();
        }

        private bool PathPartMatchesAnyRule(string pathPart) =>
            _routeRules.Any(r => this.PathPartMatchesRule(pathPart, r));

        private bool PathPartMatchesRule(string pathPart, string rule) =>
            $"{Path.DirectorySeparatorChar}{pathPart.ToUpper()}" == rule.ToUpper();

        private string[] GetPathParts(string path) =>
            path.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);

        public override string ToString() =>
            string.Join(", ", _routeRules);
    }
}
