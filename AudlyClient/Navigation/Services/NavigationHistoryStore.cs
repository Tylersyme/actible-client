﻿using AudlyClient.Navigation.Services.EventArgs;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services
{
    public class NavigationHistoryStore : INavigationHistoryStore
    {
        private const int MAX_HISTORY_LENGTH = 100;

        public string CurrentRoute { get; set; }

        public string LastRoute =>
            _navigationHistory.LastOrDefault();

        public string StartingRoute { get; private set; }

        private List<string> _navigationHistory = new List<string>();
        public IReadOnlyList<string> NavigationHistory =>
            _navigationHistory;

        public NavigationHistoryStore(string startingRoute)
        {
            this.StartingRoute = startingRoute;

            this.CurrentRoute = this.StartingRoute;
        }

        public bool TryGoBack(out string route)
        {
            route = this.LastRoute;

            if (!_navigationHistory.Any())
                return false;

            this.CurrentRoute = route;

            _navigationHistory.RemoveAt(_navigationHistory.Count - 1);

            return true;
        }

        public void Push(string route)
        {
            var routePathAndQuery = route != "/"
                ? new Uri(route).PathAndQuery
                : route;

            // Navigating to the same route is ignored when tracking history
            if (routePathAndQuery == this.CurrentRoute)
                return;

            if (this.CurrentRoute != null)
                _navigationHistory.Add(this.CurrentRoute);

            this.CurrentRoute = routePathAndQuery;

            this.EnsureMaxHistoryLength();
        }

        private void EnsureMaxHistoryLength()
        {
            if (_navigationHistory.Count <= MAX_HISTORY_LENGTH)
                return;

            _navigationHistory.RemoveAt(0);
        }
    }
}
