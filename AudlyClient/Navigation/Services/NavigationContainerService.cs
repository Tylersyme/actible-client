﻿using AudlyClient.Navigation.Services.EventArgs;
using Easy.MessageHub;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services
{
    public class NavigationContainerService : INavigationContainerService
    {
        private List<INavigationContainer> _navigationContainers = new List<INavigationContainer>();

        private INavigationContainer _currentNavigationContainer;

        private readonly IMessageHub _messageHub;

        private readonly NavigationManager _navigationManager;

        public NavigationContainerService(
            IMessageHub messageHub,
            NavigationManager navigationManager)
        {
            _messageHub = messageHub;
            _navigationManager = navigationManager;

            var topNavigationContainer = new StackNavigationContainer("home", Routes.HOME)
                .WithRouteRule("/")
                .WithChildNavigationContainer(new StackNavigationContainer("activities", Routes.ACTIVITIES)
                    .WithRouteRule("/activities"));

            _navigationContainers.Add(topNavigationContainer);

            var currentPath = new Uri(_navigationManager.Uri).PathAndQuery;

            _currentNavigationContainer = this.GetNavigationContainerOfRoute(!string.IsNullOrEmpty(currentPath)
                ? currentPath
                : "/");

            this.Subscribe();
        }

        public INavigationContainer GetNavigationContainerByName(string navigationContainerName) =>
            _navigationContainers
                .First()
                .GetByName(navigationContainerName);

        public string GetPreviousRoute() =>
            _currentNavigationContainer.LastRoute ?? 
            _currentNavigationContainer.Parent?.CurrentRoute;

        protected void OnNavigation(NavigationEventArgs eventArgs)
        {
            _currentNavigationContainer = this.GetNavigationContainerOfRoute(eventArgs.Location);

            if (!eventArgs.IsBackwards)
            {
                var route = eventArgs.Location != "/"
                    ? new Uri(eventArgs.Location).LocalPath
                    : eventArgs.Location;

                _currentNavigationContainer.Push(route);
            }
            else
            {
                _currentNavigationContainer.TryGoBack(out _);
            }
        }

        private INavigationContainer GetNavigationContainerOfRoute(string route)
        {
            var navigationContainer = default(INavigationContainer);

            _navigationContainers.First(n => n.TryGetNavigationContainerContainingRoute(route, out navigationContainer));

            return navigationContainer;
        }

        private void Subscribe()
        {
            _messageHub.Subscribe<NavigationEventArgs>(OnNavigation);
        }
    }
}
