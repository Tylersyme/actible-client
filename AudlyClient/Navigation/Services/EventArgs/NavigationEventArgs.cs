﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services.EventArgs
{
    public class NavigationEventArgs
    {
        public string Location { get; set; }

        public bool IsBackwards { get; set; }
    }
}
