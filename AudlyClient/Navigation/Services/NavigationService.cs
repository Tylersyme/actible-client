﻿using AudlyClient.Navigation.Services.EventArgs;
using Easy.MessageHub;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services
{
    public class NavigationService : INavigationService
    {
        public string Uri =>
            _navigationManager.Uri;

        private readonly NavigationManager _navigationManager;

        private readonly IMessageHub _messageHub;

        private readonly INavigationContainerService _navigationContainerService;

        public NavigationService(
            NavigationManager navigationManager, 
            IMessageHub messageHub,
            INavigationContainerService navigationContainerService)
        {
            _navigationManager = navigationManager;
            _messageHub = messageHub;
            _navigationContainerService = navigationContainerService;
        }

        public void NavigateToContainer(string navigationContainerName)
        {
            var navigationContainer = _navigationContainerService.GetNavigationContainerByName(navigationContainerName);

            var route = !string.IsNullOrEmpty(navigationContainer.CurrentRoute)
                ? navigationContainer.CurrentRoute
                : navigationContainer.StartingRoute;

            this.NavigateTo(route);
        }

        public bool GoBack()
        {
            var previousRoute = _navigationContainerService.GetPreviousRoute();

            if (previousRoute == null)
                return false;

            _messageHub.Publish(new PreNavigationEventArgs
            {
                Location = previousRoute,
                PreviousLocation = this.Uri,
                IsBackwards = true,
            });

            _navigationManager.NavigateTo(previousRoute);

            _messageHub.Publish(new NavigationEventArgs
            {
                Location = previousRoute,
                IsBackwards = true,
            });

            return true;
        }

        public void NavigateTo(string location)
        {
            _messageHub.Publish(new PreNavigationEventArgs
            {
                Location = location,
                PreviousLocation = this.Uri,
                IsBackwards = false,
            });

            _navigationManager.NavigateTo(location);

            _messageHub.Publish(new NavigationEventArgs
            {
                Location = location,
                IsBackwards = false,
            });
        }

        public Uri ToAbsoluteUri(string uri) =>
            _navigationManager.ToAbsoluteUri(uri);
    }
}
