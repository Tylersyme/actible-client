﻿using AudlyClient.Navigation.Services.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services
{
    public interface INavigationService
    {
        string Uri { get; }

        bool GoBack();

        void NavigateTo(string location);

        void NavigateToContainer(string navigationContainerName);

        Uri ToAbsoluteUri(string uri);
    }
}
