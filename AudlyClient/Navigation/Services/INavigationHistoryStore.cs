﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services
{
    public interface INavigationHistoryStore
    {
        IReadOnlyList<string> NavigationHistory { get; }

        string LastRoute { get; }

        string CurrentRoute { get; }

        string StartingRoute { get; }

        bool TryGoBack(out string location);

        void Push(string location);
    }
}
