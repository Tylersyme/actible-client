﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services
{
    public interface IRouteManager
    {
        RouteDefinition GetMatchingRouteDefinition(string url);
    }
}
