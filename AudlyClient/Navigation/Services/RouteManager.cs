﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tavis.UriTemplates;
using AudlyClient.Extensions.Utility;

namespace AudlyClient.Navigation.Services
{
    public class RouteManager : IRouteManager
    {
        private readonly IEnumerable<RouteDefinition> _routeDefinitions = new List<RouteDefinition>();

        public RouteManager(IEnumerable<RouteDefinition> routeDefinitions)
        {
            _routeDefinitions = routeDefinitions;
        }

        public RouteDefinition GetMatchingRouteDefinition(string url) =>
            _routeDefinitions.FirstOrDefault(rd => rd.Matches(url));
    }
}
