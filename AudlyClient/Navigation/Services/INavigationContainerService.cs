﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Navigation.Services
{
    public interface INavigationContainerService
    {
        INavigationContainer GetNavigationContainerByName(string navigationContainerName);

        string GetPreviousRoute();
    }
}
