﻿using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace AudlyClient.Navigation
{
    public static class Routes
    {
        static Routes()
        {
            HOME = "/";
            ACTIVITIES = "/activities";
            EXPLORE = "/explore";
            SETUP_ACTIVITY = "/setupactivity";
            PROFILE = "/profile";
            PLAN_ACTIVITY = Path.Combine(ACTIVITIES, "{0}");
            AUTHENTICATION = "/authentication";
            SIGNUP_SIGNIN = Path.Combine(AUTHENTICATION, RemoteAuthenticationActions.LogIn);
            LOGOUT = Path.Combine(AUTHENTICATION, RemoteAuthenticationActions.LogOut);
        }

        public static readonly string HOME;
        public static readonly string ACTIVITIES;
        public static readonly string SETUP_ACTIVITY;
        public static readonly string SIGNUP_SIGNIN;
        public static readonly string EXPLORE;
        public static readonly string PROFILE;
        public static readonly string PLAN_ACTIVITY;
        public static readonly string LOGOUT;
        public static readonly string AUTHENTICATION;
    }
}
