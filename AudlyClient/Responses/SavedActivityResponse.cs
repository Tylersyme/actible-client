﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Responses
{
    public class SavedActivityResponse
    {
        public Activity Activity { get; set; }

        public IEnumerable<SavedActivityDetails> SavedActivityDetails { get; set; } = new List<SavedActivityDetails>();
    }
}
