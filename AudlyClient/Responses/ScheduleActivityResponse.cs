﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Responses
{
    public class ScheduleActivityResponse
    {
        public Guid SavedActivityDetailsId { get; set; }
    }
}
