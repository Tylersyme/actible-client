﻿using AudlyClient.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Responses
{
    public class ActivityDetailsResponse
    {
        public List<Component> DetailComponents { get; set; } = new List<Component>();
    }
}
