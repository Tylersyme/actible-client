﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Responses
{
    public class PlaceAutocompleteResponse
    {
        public string PredictionText { get; set; }

        public string PlaceId { get; set; }
    }
}
