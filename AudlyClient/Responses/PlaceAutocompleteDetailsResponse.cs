﻿using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Responses
{
    public class PlaceAutocompleteDetailsResponse
    {
        public GeolocationCoordinates GeolocationCoordinates { get; set; }
    }
}
