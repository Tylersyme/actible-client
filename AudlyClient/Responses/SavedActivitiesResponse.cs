﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Responses
{
    public class SavedActivitiesResponse
    {
        public IEnumerable<SavedActivityResponse> SavedActivities { get; set; } = new List<SavedActivityResponse>();
    }
}
