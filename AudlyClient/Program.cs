using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AudlyClient.Services;
using AudlyClient.Services.Clients;
using Blazored.LocalStorage;
using AudlyClient.Services.Authentication;
using Fluxor;
using AudlyClient.Services.Interop;
using Autofac.Extras.AggregateService;
using AudlyClient.Services.Knowledge;
using AudlyClient.Services.Utility;
using AudlyClient.Services.Interop.Utility;
using AudlyClient.AppSetting;
using AudlyClient.Services.RequestBuilders.Uber;
using AudlyClient.Navigation.Services;
using Easy.MessageHub;
using AudlyClient.Services.RouteWatch;
using AudlyClient.ComponentData.Context;
using AudlyClient.Services.Clients.Builders;
using AudlyClient.Services.Clients.Google;
using Microsoft.Extensions.DependencyInjection;

namespace AudlyClient
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.ConfigureContainer(new AutofacServiceProviderFactory(ConfigureContainer));

            MappingConfiguration.ConfigureMappings();

            builder.RootComponents.Add<App>("app");

            builder.Services.AddMsalAuthentication(opts =>
            {
                builder.Configuration.Bind("AzureAdB2C", opts.ProviderOptions.Authentication);

                opts.ProviderOptions.DefaultAccessTokenScopes.Add("https://actibleorg.onmicrosoft.com/6fb66cc6-6aba-4c94-9a29-026ebea4b732/API.Access");
            });

            builder.Services.AddScoped<ActibleApiAuthorizationMessageHandler>();

            // Registers an IHttpClientFactory with an http client named ServerApi which it can create
            builder.Services
                .AddHttpClient("API", client =>
                {
                    client.BaseAddress = new Uri(Constants.API_BASE_ADDRESS);
                })
                .AddHttpMessageHandler<ActibleApiAuthorizationMessageHandler>();

            builder.Services
                .AddTransient(sp => 
                {
                    return sp
                        .GetRequiredService<IHttpClientFactory>()
                        .CreateClient("API");
                });

            builder.Services.AddBlazoredLocalStorage();

            var currentAssembly = typeof(Program).Assembly;
            builder.Services.AddFluxor(opts => opts.ScanAssemblies(currentAssembly));

            await builder.Build().RunAsync();
        }

        private static void ConfigureContainer(ContainerBuilder builder)
        {
            builder
                .Register(opts =>
                {
                    var config = opts.Resolve<IConfiguration>();

                    return config
                        .GetSection(UberSettings.SECTION_NAME)
                        .Get<UberSettings>();
                })
                .AsSelf();

            builder
                .Register(_ => MappingConfiguration.Mapper)
                .AsSelf();

            builder
                .RegisterType<ActivityClient>()
                .As<IActivityClient>();

            builder
                .RegisterType<JwtService>()
                .As<IJwtService>();

            builder
                .RegisterType<AuthTokenService>()
                .As<IAuthTokenService>();

            builder
                .RegisterType<SavedActivityService>()
                .As<ISavedActivityService>();

            builder
                .RegisterType<MDBootstrapInteropService>()
                .As<IMDBootstrapInteropService>();

            builder.RegisterAggregateService<IComponentKnowledgeAggregate>();

            builder
                .RegisterType<ImagesComponentKnowledge>()
                .As<IImagesComponentKnowlege>();

            builder
                .RegisterType<ModalInteropService>()
                .As<IModalInteropService>()
                .SingleInstance();

            builder
                .RegisterType<LocationInteropService>()
                .As<ILocationInteropService>()
                .SingleInstance();

            builder
                .RegisterType<LocationService>()
                .As<ILocationService>()
                .SingleInstance();

            builder
                .RegisterType<NavigatorInteropService>()
                .As<INavigatorInteropService>()
                .SingleInstance();

            builder
                .RegisterType<PlatformService>()
                .As<IPlatformService>()
                .SingleInstance();

            builder
                .RegisterType<WindowInteropService>()
                .As<IWindowInteropService>()
                .SingleInstance();

            builder
                .RegisterType<MapsService>()
                .As<IMapsService>()
                .SingleInstance();

            builder
                .RegisterType<UberUniversalDeepUrlBuilder>()
                .As<IUberUniversalDeepUrlBuilder>();

            builder
                .RegisterType<NavigationService>()
                .As<INavigationService>()
                .SingleInstance();

            builder
                .RegisterType<MessageHub>()
                .As<IMessageHub>()
                .SingleInstance();

            builder
                .RegisterType<NavigationContainerService>()
                .As<INavigationContainerService>()
                .SingleInstance();

            builder
                .RegisterType<NavigationBarRouteWatcher>()
                .AsSelf()
                .SingleInstance()
                .AutoActivate();

            builder
                .RegisterType<NavigationBarContext>()
                .AsSelf()
                .SingleInstance();

            builder
                .RegisterType<RouteManager>()
                .As<IRouteManager>()
                .SingleInstance();

            builder
                .RegisterType<PlacesAutocompleteRequestBuilder>()
                .As<IPlacesAutocompleteRequestBuilder>();

            builder
                .RegisterType<PlacesClient>()
                .As<IPlacesClient>()
                .SingleInstance();

            builder
                .RegisterType<HubSubscriber>()
                .As<IHubSubscriber>();

            builder
                .RegisterType<JqueryUtilityInteropService>()
                .As<IJqueryUtilityInteropService>()
                .SingleInstance();

            builder
                .RegisterType<AuthenticationManager>()
                .As<IAuthenticationManager>()
                .SingleInstance();

            builder
                .RegisterType<ActivityNavigationService>()
                .As<IActivityNavigationService>()
                .SingleInstance();

            RouteConfiguration.ConfigureRoutes(builder);
        }
    }
}
