﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ComponentData
{
    public class ActivityFilterOption
    {
        public static readonly List<ActivityFilterOption> ALL = new List<ActivityFilterOption>();

        public static readonly ActivityFilterOption ACTIVE = new ActivityFilterOption("Active", new string[]
            {
                ActivityTag.ACTIVE,
            });

        public static readonly ActivityFilterOption HUNGRY = new ActivityFilterOption("Hungry", new string[] 
            {
                ActivityTag.FOOD,
            });

        public static readonly ActivityFilterOption BORED = new ActivityFilterOption("Bored", new string[]
            {
                ActivityTag.WALK_AND_VIEW,
                ActivityTag.SHOW,
            });

        public static readonly ActivityFilterOption PAMPERED = new ActivityFilterOption("Pampered", new string[]
            {
                ActivityTag.RELAXING,
                ActivityTag.BEAUTY,
            });

        public string Name { get; set; }

        public IReadOnlyList<string> AssociatedTags { get; set; } = new List<string>();

        private ActivityFilterOption(
            string name,
            string[] associatedTags)
        {
            this.Name = name;
            this.AssociatedTags = associatedTags.ToList();

            ALL.Add(this);
        }
    }
}
