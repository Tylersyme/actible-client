﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ComponentData
{
    public class NamedItem<TValue>
    {
        public string Name { get; set; }

        public TValue Value { get; set; }
    }
}
