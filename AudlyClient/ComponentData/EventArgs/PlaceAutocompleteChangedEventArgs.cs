﻿using AudlyClient.Responses;
using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ComponentData.EventArgs
{
    public class PlaceAutocompleteChangedEventArgs
    {
        public string Value { get; set; }

        public GeolocationCoordinates PlaceCoordinates { get; set; }
    }
}
