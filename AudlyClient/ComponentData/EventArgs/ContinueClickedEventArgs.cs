﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ComponentData.EventArgs
{
    public class ContinueClickedEventArgs
    {
        public int CurrentStepNumber { get; set; }

        public object StepMetadata { get; set; }
    }
}
