﻿using AudlyClient.Navigation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ComponentData.Context
{
    public class NavigationBarContext
    {
        public bool OverrideNavigationArrowClick { get; set; }

        private Action _onNavigationArrowClicked;
        public Action OnNavigationArrowClicked
        {
            get
            {
                if (!this.OverrideNavigationArrowClick)
                    return () => _navigationService.GoBack();

                return _onNavigationArrowClicked;
            }
            set 
            {
                this.OverrideNavigationArrowClick = true;

                _onNavigationArrowClicked = value;
            }
        }

        private bool _showNavigationBar = true;
        public bool ShowNavigationBar 
        { 
            get => _showNavigationBar;
            set
            {
                _showNavigationBar = value;

                this.UpdatedEvent?.Invoke();
            }
        }

        private bool _showBottomTabs = true;
        public bool ShowBottomTabs
        {
            get => _showBottomTabs;
            set
            {
                _showBottomTabs = value;

                this.UpdatedEvent?.Invoke();
            }
        }

        private bool _hideNavigationArrow;
        public bool HideNavigationArrow
        {
            get => _hideNavigationArrow;
            set
            {
                _hideNavigationArrow = value;

                this.UpdatedEvent?.Invoke();
            }
        }

        private string _rightSideActionButtonText;
        public string RightSideActionButtonText
        {
            get => _rightSideActionButtonText;
            set
            {
                _rightSideActionButtonText = value;

                this.UpdatedEvent?.Invoke();
            }
        }

        public Action OnRightSideActionButtonClicked { get; set; }

        private string _title;
        public string Title
        {
            get => _title;
            set
            {
                _title = value;

                this.UpdatedEvent?.Invoke();
            }
        }

        public event Action UpdatedEvent;

        private readonly INavigationService _navigationService;

        public NavigationBarContext(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }
    }
}
