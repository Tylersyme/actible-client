﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ComponentData.Context
{
    public class SteppedContentContext
    {
        public int CurrentStepNumber { get; set; } = 0;

        public event Action<int> StepChangedEvent;

        public object CurrentStepMetaData { get; set; }

        public void ChangeStep(int stepNumber)
        {
            this.CurrentStepNumber = stepNumber;

            this.StepChangedEvent?.Invoke(stepNumber);
        }
    }
}
