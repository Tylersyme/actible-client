﻿using AudlyClient.Services.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Extensions
{
    public static class CachedItemExtensions
    {
        public static bool IsExpired(this ICachedItem cachedItem) =>
            DateTime.Now > cachedItem.ExpirationDate;
    }
}
