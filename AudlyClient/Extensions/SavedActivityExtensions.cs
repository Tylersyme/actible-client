﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Extensions
{
    public static class SavedActivityExtensions
    {
        public static bool IsScheduled(this SavedActivity savedActivity) =>
            savedActivity.SavedActivityDetails != null;

        public static bool IsInProgress(this SavedActivity savedActivity) =>
            savedActivity.IsScheduled() &&
            DateTime.Now > savedActivity.SavedActivityDetails.StartTime &&
            DateTime.Now < savedActivity.SavedActivityDetails.EndTime;

        public static bool IsFinished(this SavedActivity savedActivity) =>
            savedActivity.IsScheduled() &&
            DateTime.Now > savedActivity.SavedActivityDetails.EndTime;

        public static IOrderedEnumerable<SavedActivity> GetScheduledActivitiesOrderedByDate(this IEnumerable<SavedActivity> savedActivities) =>
            savedActivities
                .Where(a => a.IsScheduled())
                .OrderBy(a => a.SavedActivityDetails.StartTime);
    }
}
