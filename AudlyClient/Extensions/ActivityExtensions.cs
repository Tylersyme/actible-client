﻿using AudlyClient.Models;
using AudlyClient.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace AudlyClient.Extensions
{
    public static class ActivityExtensions
    {
        public static TComponent GetComponent<TComponent>(this Activity activity) where TComponent : Component =>
            activity.Components
                .OfType<TComponent>()
                .FirstOrDefault();

        public static bool HasComponent<TComponent>(this Activity activity) where TComponent : Component =>
            activity.GetComponent<TComponent>() != null;
    }
}
