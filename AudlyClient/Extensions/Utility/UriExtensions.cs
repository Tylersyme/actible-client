﻿using Flurl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Extensions.Utility
{
    public static class UriExtensions
    {
        public static bool TryGetQueryStringValue(this Uri uri, string queryStringName, out string queryStringValue)
        {
            var url = new Url(uri.AbsoluteUri);

            queryStringValue = url.QueryParams.FirstOrDefault(qp => qp.Name.EqualsIgnoreCase(queryStringName))?.Value.ToString();

            return queryStringValue != null;
        }
    }
}
