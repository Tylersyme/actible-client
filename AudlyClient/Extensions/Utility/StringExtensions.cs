﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace AudlyClient.Extensions.Utility
{
    public static class StringExtensions
    {
        public static StringContent ToJsonContent(this string value) =>
            new StringContent(value, Encoding.UTF8, MediaTypeNames.Application.Json);

        public static string ToCssId(this string value) =>
            $"#{value}";

        public static string ToCssClass(this string value) =>
            $".{value}";

        public static bool EqualsIgnoreCase(this string value, string toCompare) =>
            string.Compare(value, toCompare, StringComparison.OrdinalIgnoreCase) == 0;
    }
}
