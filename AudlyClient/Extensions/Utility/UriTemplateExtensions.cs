﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tavis.UriTemplates;

namespace AudlyClient.Extensions.Utility
{
    public static class UriTemplateExtensions
    {
        public static bool Matches(this UriTemplate uriTemplate, string url)
        {
            var regex = UriTemplate.CreateMatchingRegex(uriTemplate.ToString());

            return Regex.IsMatch(url, regex);
        }
    }
}
