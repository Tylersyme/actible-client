﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Extensions.Utility
{
    public static class JavascriptExtensions
    {
        public static string ToJavascriptDateString(this DateTime dateTime) =>
            dateTime.ToString("yyyy-MM-ddTHH:mm:ss");
    }
}
