﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.AppSetting
{
    public class UberSettings
    {
        public const string SECTION_NAME = "Uber";

        public string ClientId { get; set; }
    }
}
