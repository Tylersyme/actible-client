﻿using AudlyClient.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Validation.Forms
{
    public class LoginValidator : AbstractValidator<Login>
    {
        public LoginValidator()
        {
            base
                .RuleFor(x => x.UsernameOrEmail)
                .NotEmpty()
                .WithMessage("Required");

            base
                .RuleFor(x => x.PlainTextPassword)
                .NotEmpty()
                .WithMessage("Required");
        }
    }
}
