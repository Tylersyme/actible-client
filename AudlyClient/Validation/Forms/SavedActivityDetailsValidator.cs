﻿using AudlyClient.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Validation.Forms
{
    public class SavedActivityDetailsValidator : AbstractValidator<SavedActivityDetails>
    {
        public SavedActivityDetailsValidator()
        {
            base
                .RuleFor(x => x.StartTime)
                .NotEmpty()
                .WithMessage("Please choose a starting date.");

            base
                .RuleFor(x => x.StartTime)
                .GreaterThanOrEqualTo(DateTime.Now)
                .WithMessage("The date is in the past. Choose a future date.");

            base
                .RuleFor(x => x.EndTime)
                .NotEmpty()
                .WithMessage("Please choose an ending date.");

            base
                .RuleFor(x => x.EndTime)
                .Must((d, x) => x >= d.StartTime)
                .WithMessage("The end date must be greater than the start date.");
        }
    }
}
