﻿using AudlyClient.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Validation.Forms
{
    public class LocationValidator : AbstractValidator<LocationViewModel>
    {
        public LocationValidator()
        {
            base
                .RuleFor(x => x.PlaceName)
                .NotEmpty()
                .WithMessage("Enter a location.");

            base
                .RuleFor(x => x.Coordinates)
                .NotNull()
                .When(x => !string.IsNullOrEmpty(x.PlaceName))
                .WithMessage("A place with that name could not be found.\nChoose an option from the list.");
        }
    }
}
