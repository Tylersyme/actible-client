﻿using AudlyClient.Models;
using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Validation.Forms
{
    public class SignUpValidator : AbstractValidator<SignUp>
    {
        private int _minimumPasswordLength = 6;

        public SignUpValidator()
        {
            base
                .RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Required");

            base
                .RuleFor(x => x.Email)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible)
                .WithMessage("Must be a valid email address");

            base
                .RuleFor(x => x.PlainTextPassword)
                .NotEmpty()
                .WithMessage("Required");

            base
                .RuleFor(x => x.PlainTextPassword)
                .MinimumLength(_minimumPasswordLength)
                .WithMessage($"Must be at least {_minimumPasswordLength} characters long");
        }
    }
}
