﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Inputs
{
    public partial class Button
    {
        [Parameter]
        public RenderFragment ChildContent { get; set; }

        [Parameter]
        public bool Secondary { get; set; }

        [Parameter]
        public bool Rectangular { get; set; }

        [Parameter]
        public string CustomButtonClass { get; set; }

        [Parameter]
        public bool Short { get; set; }

        [Parameter]
        public bool Icon { get; set; }

        [Parameter]
        public bool Flat { get; set; }

        [Parameter]
        public bool Submit { get; set; }

        [Parameter]
        public bool Anchor { get; set; }

        [Parameter]
        public bool Outline { get; set; }

        [Parameter]
        public string Class { get; set; }

        [Parameter]
        public string Id { get; set; }

        [Parameter]
        public string Href { get; set; }

        [Parameter]
        public string Target { get; set; }

        [Parameter]
        public string Text { get; set; }

        [Parameter(CaptureUnmatchedValues = true)]
        public IDictionary<string, object> OtherAttributes { get; set; } = new Dictionary<string, object>();

        [Parameter]
        public EventCallback<MouseEventArgs> OnClick { get; set; }

        private bool UseAnchorTag =>
            this.Anchor || this.Icon;

        private string Classes =>
            string.Join(' ', new string[]
            {
                this.ButtonClass,
                this.ButtonThemeClass,
                this.ShortClass,
                this.Class,
                this.FlatClass,
                this.ShapeClass,
            });

        private string ButtonThemeClass
        {
            get
            {
                if (!string.IsNullOrEmpty(this.CustomButtonClass))
                    return this.CustomButtonClass;

                var cssClass = this.Secondary
                    ? $"btn{(this.Outline ? "-outline" : "")}-secondary" : $"btn{(this.Outline ? "-outline" : "")}-primary";

                return cssClass;
            }
        }

        private string ShortClass =>
            this.Short
                ? "py-2"
                : string.Empty;

        private string ButtonClass =>
            this.Icon
                ? "btn-floating"
                : "btn";

        private string FlatClass =>
            this.Flat
                ? "btn-flat"
                : string.Empty;

        private string ButtonType =>
            this.Submit
                ? "submit"
                : "button";

        private string ShapeClass =>
            this.Rectangular
                ? string.Empty
                : "btn-rounded";

        private async Task OnClickedAsync(MouseEventArgs eventArgs) =>
            await this.OnClick.InvokeAsync(eventArgs);
    }
}
