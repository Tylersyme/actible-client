﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Inputs
{
    public partial class IconButton
    {
        [Parameter]
        public string IconClass { get; set; }

        [Parameter]
        public string ButtonClass { get; set; }

        [Parameter]
        public string Class { get; set; }

        [Parameter]
        public string ColorClass { get; set; }

        [Parameter]
        public string Href { get; set; }

        [Parameter]
        public string Target { get; set; }

        [Parameter]
        public string FontSize { get; set; }

        [Parameter]
        public bool Floating { get; set; } = false;

        [Parameter]
        public EventCallback<MouseEventArgs> OnClick { get; set; }

        private async Task OnClickedAsync(MouseEventArgs eventArgs) =>
            await this.OnClick.InvokeAsync(eventArgs);

        private string Classes =>
            string.Join(' ', new string[]
            {
                this.Class,
                this.IconClass,
                string.IsNullOrEmpty(this.ColorClass)
                    ? "text-grey"
                    : this.ColorClass,
            });
    }
}
