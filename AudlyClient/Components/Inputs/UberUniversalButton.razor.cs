﻿using AudlyClient.Services.Interop.Models;
using AudlyClient.Services.RequestBuilders.Uber;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Inputs
{
    public partial class UberUniversalButton : Button
    {
        [Inject]
        private IUberUniversalDeepUrlBuilder UrlBuilder { get; set; }

        [Parameter]
        public string PickupNickname { get; set; }

        [Parameter]
        public string DropoffNickname { get; set; }

        [Parameter]
        public GeolocationCoordinates PickupCoordinates { get; set; }

        [Parameter]
        public GeolocationCoordinates DropoffCoordinates { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.SetDefaults();

            await base.OnInitializedAsync();
        }

        private RenderFragment RenderBase() =>
            builder => base.BuildRenderTree(builder);

        private void SetDefaults()
        {
            this.UrlBuilder.Create(this.PickupNickname, this.DropoffNickname);

            if (this.PickupCoordinates != null)
                this.UrlBuilder.WithPickupCoordinates(this.PickupCoordinates);

            if (this.DropoffCoordinates != null)
                this.UrlBuilder.WithDropoffCoordinates(this.DropoffCoordinates);

            base.CustomButtonClass = "btn-uber";
            base.Anchor = true;
            base.Href = this.UrlBuilder.Build();
            base.Text = "Schedule a ride with Uber";
            base.Target = "_blank";
        }
    }
}
