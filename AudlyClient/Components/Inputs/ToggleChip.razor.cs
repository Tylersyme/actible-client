﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Inputs
{
    public partial class ToggleChip
    {
        [Parameter]
        public string Text { get; set; }

        [Parameter]
        public bool IsSelected { get; set; }

        [Parameter]
        public EventCallback<bool> OnToggled { get; set; }

        private async Task OnClickAsync()
        {
            this.IsSelected = !this.IsSelected;

            await this.OnToggled.InvokeAsync(this.IsSelected);
        }
    }
}
