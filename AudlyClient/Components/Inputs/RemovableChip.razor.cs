﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Inputs
{
    public partial class RemovableChip
    {
        [Parameter]
        public string Text { get; set; }

        [Parameter]
        public EventCallback<string> OnRemoved { get; set; }

        private async Task OnRemovedClickedAsync()
        {
            await this.OnRemoved.InvokeAsync(this.Text);
        }
    }
}
