﻿using AudlyClient.ComponentData;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Inputs
{
    public partial class TagSelector<TValue>
    {
        [Parameter]
        public string ButtonText { get; set; }

        [Parameter]
        public IEnumerable<NamedItem<TValue>> Options { get; set; } = new List<NamedItem<TValue>>();

        [Parameter]
        public IList<TValue> SelectedOptions { get; set; } = new List<TValue>();

        [Parameter]
        public EventCallback<IList<TValue>> SelectedOptionsChanged { get; set; }

        private List<TValue> UnselectedOptions =>
            this.Options
                .Where(o => !this.SelectedOptions.Any(so => so.Equals(o.Value)))
                .Select(o => o.Value)
                .ToList();

        private async Task OnOptionAddedAsync(TValue selectedOption)
        {
            this.SelectedOptions.Add(selectedOption);

            await this.SelectedOptionsChanged.InvokeAsync(this.SelectedOptions);
        }

        private async Task OnOptionRemovedAsync(TValue removedOption)
        {
            this.SelectedOptions.Remove(removedOption);

            await this.SelectedOptionsChanged.InvokeAsync(this.SelectedOptions);
        }

        private string GetOptionName(TValue option) =>
            this.Options.FirstOrDefault(o => o.Value.Equals(option))?.Name;
    }
}
