﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Forms.Inputs
{
    public partial class ErrorText
    {
        [Parameter]
        public RenderFragment ChildContent { get; set; }
    }
}
