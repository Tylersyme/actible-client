﻿using AudlyClient.Extensions.Utility;
using AudlyClient.Services.Interop;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AudlyClient.Components.Forms.Inputs
{
    public partial class ValidatedTimepicker
    {
        private string _timepickerDivId = Guid.NewGuid().ToString();

        private string _timepickerInputId = Guid.NewGuid().ToString();

        [Parameter]
        public string Label { get; set; }

        [Parameter]
        public DateTime Value { get; set; }

        [Parameter]
        public EventCallback<DateTime> ValueChanged { get; set; }

        [Parameter]
        public string Format { get; set; }

        [Parameter]
        public Expression<Func<DateTime>> For { get; set; }

        [Parameter]
        public DateTime? Minimum { get; set; }

        [Inject]
        private IMDBootstrapInteropService MDBootstrapInteropService { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender)
                return;

            var objectReference = DotNetObjectReference.Create(this);

            await this.MDBootstrapInteropService.InitializeTimepickersAsync(
                _timepickerDivId.ToCssId(),
                _timepickerInputId.ToCssId(),
                objectReference,
                nameof(OnValueChangedAsync));
        }

        [JSInvokable]
        public async Task OnValueChangedAsync(string value)
        {
            var isValid = DateTime.TryParseExact(
                value, 
                this.Format, 
                CultureInfo.CurrentCulture, 
                DateTimeStyles.AdjustToUniversal, 
                out var parsedDateTime);

            if (!isValid)
                return;

            this.Value = this.Value.Date.Add(parsedDateTime.TimeOfDay);

            await this.ValueChanged.InvokeAsync(this.Value);
        }
    }
}
