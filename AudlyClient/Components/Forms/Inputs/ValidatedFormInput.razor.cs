﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AudlyClient.Components.Forms.Inputs
{
    public partial class ValidatedFormInput<TValue>
    {
        [Parameter]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        [Parameter]
        public Expression<Func<TValue>> For { get; set; }

        [Parameter]
        public string Class { get; set; }

        [Parameter]
        public string Value { get; set; }

        [Parameter]
        public string Label { get; set; }

        [Parameter]
        public bool Large { get; set; }

        [Parameter]
        public string Type { get; set; } = "text";

        [Parameter]
        public EventCallback<string> ValueChanged { get; set; }

        private async Task OnInputAsync(ChangeEventArgs eventArgs)
        {
            this.Value = eventArgs.Value.ToString();

            await this.ValueChanged.InvokeAsync(this.Value);
        }
    }
}
