﻿using AudlyClient.Extensions.Utility;
using AudlyClient.Services.Interop;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudlyClient.Services.Clients.Builders;
using AudlyClient.Services.Clients.Google;
using AudlyClient.Requests;
using Microsoft.AspNetCore.Components.Web;
using AudlyClient.Responses;
using AudlyClient.ComponentData.EventArgs;
using AudlyClient.Services.Interop.Models;
using AudlyClient.Services.Interop.Utility;
using Microsoft.JSInterop;

namespace AudlyClient.Components.Forms.Inputs
{
    public partial class PlaceAutocompleteInput
    {
        private static readonly TimeSpan SESSION_LIFETIME_DURATION = TimeSpan.FromMinutes(2);

        [Parameter]
        public string Value { get; set; }

        [Parameter]
        public EventCallback<string> ValueChanged { get; set; }

        [Parameter]
        public EventCallback<PlaceAutocompleteChangedEventArgs> PlaceChanged { get; set; }

        [Parameter]
        public string Class { get; set; }

        [Inject]
        private IMDBootstrapInteropService MDBootstrapInteropService { get; set; }

        [Inject]
        private IJqueryUtilityInteropService JqueryUtilityInteropService { get; set; }

        [Inject]
        private IPlacesAutocompleteRequestBuilder AutocompleteRequestBuilder { get; set; }

        [Inject]
        private IPlacesClient PlacesClient { get; set; }

        private string _id = Guid.NewGuid().ToString();

        private IEnumerable<PlaceAutocompleteResponse> _currentPlaceAutocompleteResponses;

        private (DateTime SessionExpiration, PlaceAutocompleteRequest Request) _previousAutocompleteRequestCache;

        private string _previousPlaceNameValue;

        protected override void OnInitialized()
        {
            _previousPlaceNameValue = this.Value;
        }

        /// <summary>
        /// HACK: MD Bootstrap does not trigger a change event when selecting an item from the list.
        /// This provides a way for external services to force this input to update.
        /// </summary>
        public async Task ForceTriggerChangeAsync() =>
            await this.JqueryUtilityInteropService.TriggerChangeAsync(_id.ToCssId());

        private async Task OnInputAsync(ChangeEventArgs eventArgs)
        {
            this.Value = eventArgs.Value.ToString();

            await this.ValueChanged.InvokeAsync(this.Value);

            if (this.Value.Length > 2)
                await this.UpdateAutocompleteAsync(this.Value);
            else
                await this.ClearAutocompleteAsync();
        }

        private async Task OnChangedAsync(ChangeEventArgs eventArgs) =>
            await this.TriggerChangeAsync(eventArgs.Value.ToString(), false);

        private async Task TriggerChangeAsync(string value, bool fromJavascript)
        {
            if (!fromJavascript && this.Value == value)
                return;

            this.Value = value;

            await this.ValueChanged.InvokeAsync(this.Value);

            var matchingAutcompleteResponse = this.GetMatchingAutcompleteResponse(this.Value);

            if (matchingAutcompleteResponse != null && this.Value != _previousPlaceNameValue)
            {
                var result = await this.PlacesClient.GetAutocompleteDetailsAsync(new PlaceAutocompleteDetailsRequest
                {
                    SessionToken = _previousAutocompleteRequestCache.Request.SessionToken,
                    PlaceId = matchingAutcompleteResponse.PlaceId,
                });

                var changedEventArgs = new PlaceAutocompleteChangedEventArgs
                {
                    Value = this.Value,
                    PlaceCoordinates = result.GeolocationCoordinates,
                };

                // Previous request's session token is no longer valid after getting place details
                _previousAutocompleteRequestCache.Request = null;

                await this.PlaceChanged.InvokeAsync(changedEventArgs);
            }

            _previousPlaceNameValue = this.Value;
        }

        private async Task UpdateAutocompleteAsync(string input)
        {
            var autocompleteRequestCache = default((DateTime SessionExpiration, PlaceAutocompleteRequest Request));

            if (_previousAutocompleteRequestCache.Request != null && DateTime.UtcNow <= _previousAutocompleteRequestCache.SessionExpiration)
            {
                var request = this.AutocompleteRequestBuilder
                    .ContinueAsExistingSession(_previousAutocompleteRequestCache.Request, input)
                    .Build();

                autocompleteRequestCache = (_previousAutocompleteRequestCache.SessionExpiration, request);
            }
            else
            {
                var request = this.AutocompleteRequestBuilder
                    .CreateAsNewSession()
                    .WithInput(input)
                    .Build();

                autocompleteRequestCache = (DateTime.UtcNow.Add(SESSION_LIFETIME_DURATION), request);
            }

            _currentPlaceAutocompleteResponses = await this.PlacesClient.GetAutocompletePredictionsAsync(autocompleteRequestCache.Request);

            var autocompleteOptions = _currentPlaceAutocompleteResponses.Select(p => p.PredictionText);

            await this.SetAutocompleteOptionsAsync(autocompleteOptions);

            _previousAutocompleteRequestCache = autocompleteRequestCache;
        }

        private PlaceAutocompleteResponse GetMatchingAutcompleteResponse(string placeName) =>
            _currentPlaceAutocompleteResponses?.FirstOrDefault(r => r.PredictionText.EqualsIgnoreCase(placeName));

        private async Task ClearAutocompleteAsync() =>
            await this.SetAutocompleteOptionsAsync(Enumerable.Empty<string>());

        private async Task SetAutocompleteOptionsAsync(IEnumerable<string> autocompleteOptions) =>
            await this.MDBootstrapInteropService.SetAutocompleteDataAndShowAsync(_id.ToCssId(), autocompleteOptions);
    }
}
