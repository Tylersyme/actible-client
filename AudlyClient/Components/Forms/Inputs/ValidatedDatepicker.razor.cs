﻿using AudlyClient.Extensions.Utility;
using AudlyClient.Services.Interop;
using AudlyClient.Services.Interop.Options;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AudlyClient.Components.Forms.Inputs
{
    public partial class ValidatedDatepicker
    {
        private string _datepickerDivId = Guid.NewGuid().ToString();

        private string _datepickerInputId = Guid.NewGuid().ToString();

        [Parameter]
        public string Label { get; set; }

        [Parameter]
        public DateTime Value { get; set; }

        [Parameter]
        public EventCallback<DateTime> ValueChanged { get; set; }

        [Parameter]
        public string Format { get; set; }

        [Parameter]
        public string JavascriptFormat { get; set; }

        [Parameter]
        public Expression<Func<DateTime>> For { get; set; }

        [Inject]
        private IMDBootstrapInteropService MDBootstrapInteropService { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender)
                return;

            var objectReference = DotNetObjectReference.Create(this);

            await this.MDBootstrapInteropService.InitializeDatepickersAsync(
                _datepickerDivId.ToCssId(),
                _datepickerInputId.ToCssId(),
                objectReference,
                nameof(OnValueChangedAsync),
                new MDBootstrapDatepickerOptions
                {
                    MinimumDate = DateTime.Now.Date,
                    DateFormat = this.JavascriptFormat,
                });
        }

        [JSInvokable]
        public async Task OnValueChangedAsync(string value)
        {
            var isValid = DateTime.TryParseExact(
                value, 
                this.Format, 
                CultureInfo.CurrentCulture, 
                DateTimeStyles.AdjustToUniversal, 
                out var parsedDateTime);

            if (!isValid)
                return;

            this.Value = parsedDateTime.Date.Add(this.Value.TimeOfDay);

            await this.ValueChanged.InvokeAsync(this.Value);
        }
    }
}
