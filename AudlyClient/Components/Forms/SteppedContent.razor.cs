﻿using AudlyClient.ComponentData;
using AudlyClient.ComponentData.Context;
using AudlyClient.ComponentData.EventArgs;
using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Forms
{
    public partial class SteppedContent
    {
        [CascadingParameter]
        public NavigationBarContext NavigationBarContext { get; set; }

        [Parameter]
        public bool HidePrevious { get; set; } = false;

        [Parameter]
        public RenderFragment Step0 { get; set; }

        [Parameter]
        public RenderFragment Step1 { get; set; }

        [Parameter]
        public RenderFragment Step2 { get; set; }

        [Parameter]
        public RenderFragment Step3 { get; set; }

        [Parameter]
        public Predicate<ContinueClickedEventArgs> CanContinue { get; set; }

        [Parameter]
        public EventCallback OnCompleted { get; set; }

        [Inject]
        private INavigationService NavigationService { get; set; }

        private bool DisablePrevious =>
            _steppedContentContext.CurrentStepNumber <= 0;

        private bool IsLastStep =>
            _steppedContentContext.CurrentStepNumber >= this.GetLargestStepNumber();

        private SteppedContentContext _steppedContentContext = new SteppedContentContext();

        protected override Task OnInitializedAsync()
        {
            _steppedContentContext.ChangeStep(0);

            this.NavigationBarContext.OnNavigationArrowClicked = this.Previous;
            this.NavigationBarContext.RightSideActionButtonText = "Cancel";
            this.NavigationBarContext.OnRightSideActionButtonClicked = () => this.NavigationService.NavigateTo(Routes.EXPLORE);

            return Task.CompletedTask;
        }

        private async Task OnProceedClickedAsync()
        {
            if (!this.IsLastStep)
                await this.ContinueAsync();
            else
                await this.CompleteAsync();
        }

        private Task ContinueAsync()
        {
            var canContinue = this.GetCanContinue();

            if (!canContinue)
                return Task.CompletedTask;

            _steppedContentContext.ChangeStep(_steppedContentContext.CurrentStepNumber + 1);

            return Task.CompletedTask;
        }

        private async Task CompleteAsync()
        {
            var canContinue = this.GetCanContinue();

            if (!canContinue)
                return;

            await this.OnCompleted.InvokeAsync(null);
        }

        private Task OnPreviousClickedAsync()
        {
            this.Previous();

            return Task.CompletedTask;
        }

        private void Previous() =>
            _steppedContentContext.ChangeStep(_steppedContentContext.CurrentStepNumber - 1);

        private bool GetCanContinue() =>
            this.CanContinue(new ContinueClickedEventArgs
            {
                CurrentStepNumber = _steppedContentContext.CurrentStepNumber,
                StepMetadata = _steppedContentContext.CurrentStepMetaData,
            });

        private int GetLargestStepNumber()
        {
            if (this.Step0 == null)
                return -1;
            else if (this.Step1 == null)
                return 0;
            else if (this.Step2 == null)
                return 1;
            else if (this.Step3 == null)
                return 2;

            throw new NotImplementedException();
        }
    }
}
