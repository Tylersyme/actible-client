﻿using AudlyClient.ComponentData.EventArgs;
using AudlyClient.Models;
using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using AudlyClient.Services;
using AudlyClient.Store.ActivitySetup.Actions;
using AudlyClient.Store.SelectedActivity;
using Fluxor;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Forms.ActivitySetup
{
    public partial class ActivitySetupForm
    {
        private SavedActivityDetails _savedActivityDetails;

        [Inject]
        private INavigationService NavigationService { get; set; }

        [Inject]
        private IState<SelectedActivityState> SelectedActivityState { get; set; }

        [Inject]
        private ISavedActivityService ActivityCacheService { get; set; }

        [Inject]
        private IDispatcher Dispatcher { get; set; }

        private Activity Activity =>
            this.SelectedActivityState.Value.Activity;

        protected override Task OnInitializedAsync()
        {
            _savedActivityDetails = new SavedActivityDetails
            {
                StartTime = DateTime.Now.AddHours(1),
                EndTime = DateTime.Now.AddHours(2),
            };

            this.Dispatcher.Dispatch(new SetSetupActivityAction { Activity = this.Activity });

            return Task.CompletedTask;
        }

        private bool ValidateStep(EditContext editContext, ContinueClickedEventArgs eventArgs)
        {
            var isFormValid = editContext.Validate();

            if (isFormValid)
                return true;

            var fieldNames = (string[])eventArgs.StepMetadata;

            foreach (var fieldName in fieldNames)
            {
                var fieldIdentifier = editContext.Field(fieldName);

                var isFieldValid = !editContext
                    .GetValidationMessages(fieldIdentifier)
                    .Any();

                if (!isFieldValid)
                    return false;
            }

            return true;
        }

        private async Task SubmitAsync()
        {
            await this.ActivityCacheService.ScheduleActivityAsync(this.Activity, _savedActivityDetails);

            this.Dispatcher.Dispatch(new SetSavedActivityDetailsAction { SavedActivityDetails = _savedActivityDetails });

            this.NavigationService.NavigateTo(Routes.ACTIVITIES);
        }
    }
}
