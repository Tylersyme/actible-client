﻿using AudlyClient.ComponentData;
using AudlyClient.ComponentData.Context;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Forms
{
    public partial class Step<TMetadata> : IDisposable
    {
        [CascadingParameter]
        public SteppedContentContext SteppedContentContext { get; set; }

        [CascadingParameter]
        public NavigationBarContext NavigationBarContext { get; set; }

        [Parameter]
        public string Title { get; set; }

        [Parameter]
        public string Class { get; set; }

        [Parameter]
        public int StepNumber { get; set; }

        [Parameter]
        public RenderFragment ChildContent { get; set; }

        [Parameter]
        public TMetadata Metadata { get; set; }

        private bool IsFirstStep =>
            this.StepNumber == 0;

        protected override void OnInitialized()
        {
            this.Subscribe();

            if (this.IsFirstStep)
                this.ShowStep();
        }

        protected void OnStepChanged(int stepNumber)
        {
            if (this.StepNumber != stepNumber)
                return;

            this.ShowStep();
        }

        private void ShowStep()
        {
            this.SteppedContentContext.CurrentStepMetaData = this.Metadata;

            this.UpdateNavigationBar();
        }

        private void Subscribe() =>
            this.SteppedContentContext.StepChangedEvent += OnStepChanged;

        private void Unsubscribe() =>
            this.SteppedContentContext.StepChangedEvent -= OnStepChanged;

        private void UpdateNavigationBar()
        {
            this.NavigationBarContext.Title = this.Title;

            this.NavigationBarContext.HideNavigationArrow = this.IsFirstStep;
        }

        public void Dispose()
        {
            this.Unsubscribe();
        }
    }
}
