﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components
{
    public partial class Card
    {
        [Parameter]
        public RenderFragment Body { get; set; }

        [Parameter]
        public RenderFragment Placeholder { get; set; }

        [Parameter]
        public RenderFragment AboveBody { get; set; }

        [Parameter]
        public string CardClass { get; set; }

        [Parameter]
        public string CardBodyClass { get; set; }

        [Parameter]
        public bool ShowPlaceholder { get; set; }

        [Parameter]
        public string CardAccentColorClass { get; set; }

    }
}
