﻿using AudlyClient.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Modals
{
    public partial class ActivityModal
    {
        [Parameter]
        public Activity Activity { get; set; }
    }
}
