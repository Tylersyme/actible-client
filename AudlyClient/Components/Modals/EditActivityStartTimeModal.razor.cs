﻿using AudlyClient.Extensions.Utility;
using AudlyClient.Services.Interop;
using AudlyClient.ViewModels;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Modals
{
    public partial class EditActivityStartTimeModal
    {
        [Inject]
        private IModalInteropService ModalInteropService { get; set; }

        [Parameter]
        public string Id { get; set; }

        [Parameter]
        public string Title { get; set; }

        [Parameter]
        public SavedActivityDetailsViewModel Model { get; set; }

        [Parameter]
        public EventCallback<SavedActivityDetailsViewModel> OnSubmit { get; set; }

        public async Task OpenAsync() =>
            await this.ModalInteropService.OpenModalAsync(this.Id.ToCssId());

        public async Task CloseAsync() =>
            await this.ModalInteropService.CloseModalAsync(this.Id.ToCssId());

        private async Task OnSubmitAsync() =>
            await this.OnSubmit.InvokeAsync(this.Model);
    }
}
