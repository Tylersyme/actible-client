﻿using AudlyClient.Services.Interop;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Modals
{
    public partial class ConfirmDeleteModal
    {
        [Parameter]
        public string Id { get; set; }

        [Parameter]
        public EventCallback OnConfirmed { get; set; }

        [Inject]
        private IModalInteropService ModalInteropService { get; set; }

        private async Task ConfirmClickedAsync() =>
            await this.OnConfirmed.InvokeAsync(null);

        public async Task OpenAsync() =>
            await this.ModalInteropService.OpenModalAsync($"#{this.Id}");
    }
}
