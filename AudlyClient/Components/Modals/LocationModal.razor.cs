﻿using AudlyClient.ComponentData.EventArgs;
using AudlyClient.Components.Forms.Inputs;
using AudlyClient.Extensions.Utility;
using AudlyClient.Services.EventArgs;
using AudlyClient.Services.Interop;
using AudlyClient.Services.Utility;
using AudlyClient.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Modals
{
    public partial class LocationModal : IDisposable
    {
        [Parameter]
        public string Id { get; set; }

        [Parameter]
        public string Title { get; set; }

        [Parameter]
        public LocationViewModel Model { get; set; } = new LocationViewModel();

        [Parameter]
        public EventCallback<LocationViewModel> OnSubmit { get; set; }

        [Inject]
        private IModalInteropService ModalInteropService { get; set; }

        [Inject]
        private ILocationService LocationService { get; set; }

        [Inject]
        private IHubSubscriber HubSubscriber { get; set; }

        private bool _showCurrentLocationNotFoundError;

        private PlaceAutocompleteInput _placeAutocompleteInput;

        public async Task OpenAsync()
        {
            this.Subscribe();

            await this.ModalInteropService.OpenModalAsync(this.Id.ToCssId());
        }

        public async Task CloseAsync()
        {
            this.HubSubscriber.UnsubscribeAll();

            await this.ModalInteropService.CloseModalAsync(this.Id.ToCssId());
        }

        protected async void OnLocationRequestResponded(LocationRequestRespondedEventArgs eventArgs)
        {
            _showCurrentLocationNotFoundError = !eventArgs.IsSuccessful;

            if (!eventArgs.IsSuccessful)
                return;

            this.Model.Coordinates = eventArgs.Coordinates;
            this.Model.PlaceName = null;

            await this.OnSubmitAsync();
        }

        private async Task OnSubmitAsync()
        {
            await this.CloseAsync();

            await this.OnSubmit.InvokeAsync(this.Model);
        }

        private async Task SubmitIfValidAsync(EditContext editContext)
        {
            await _placeAutocompleteInput.ForceTriggerChangeAsync();

            base.StateHasChanged();

            var isValid = editContext.Validate();

            if (isValid)
                await this.OnSubmitAsync();
        }

        private void OnPlaceChanged(PlaceAutocompleteChangedEventArgs eventArgs)
        {
            this.Model.Coordinates = eventArgs.PlaceCoordinates;
            this.Model.PlaceName = eventArgs.Value;
        }

        private async Task OnUseMyLocationClickedAsync()
        {
            await this.LocationService.RequestAndStoreCurrentLocationAsync();
        }

        public void Dispose() =>
            this.HubSubscriber.UnsubscribeAll();

        private void Subscribe() =>
            this.HubSubscriber.Subscribe<LocationRequestRespondedEventArgs>(OnLocationRequestResponded);
    }
}
