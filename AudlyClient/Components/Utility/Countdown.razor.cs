﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AudlyClient.Components.Utility
{
    public partial class Countdown
    {
        [Parameter]
        public DateTime TargetDate { get; set; }

        [Parameter]
        public string PrefixText { get; set; }

        [Parameter]
        public EventCallback OnCountdownCompleted { get; set; }

        private double _remainingMilliseconds;

        private Timer _countdownTimer;

        protected override Task OnInitializedAsync()
        {
            this.UpdateRemainingMilliseconds();

            _countdownTimer = new Timer(TimeSpan.FromSeconds(1).TotalMilliseconds);
            _countdownTimer.Elapsed += async (s, e) => await this.OnIntervalElapsedAsync(s, e);
            _countdownTimer.AutoReset = true;

            _countdownTimer.Start();

            return Task.CompletedTask;
        }

        private async Task OnIntervalElapsedAsync(object source, ElapsedEventArgs eventArgs)
        {
            this.UpdateRemainingMilliseconds();

            if (this.TargetDate < DateTime.Now)
            {
                _countdownTimer.Stop();

                await this.OnCountdownCompleted.InvokeAsync(null);
            }

            base.StateHasChanged();
        }

        private void UpdateRemainingMilliseconds() =>
            _remainingMilliseconds = (this.TargetDate - DateTime.Now).TotalMilliseconds;

        private string GetFormattedCountdown()
        {
            var span = TimeSpan.FromMilliseconds(_remainingMilliseconds);

            var countdownBuilder = new StringBuilder(this.PrefixText);

            if (span.Days > 0)
            {
                countdownBuilder.Append($"{span.Days} day{(span.Days == 1 ? string.Empty : "s")}");
            }
            else if (span.Minutes > 0)
            {
                if (span.Hours != 0)
                    countdownBuilder.Append($"{span.Hours} hour{(span.Hours == 1 ? string.Empty : "s")}, ");

                if (span.Minutes != 0)
                    countdownBuilder.Append($"{span.Minutes} minute{(span.Minutes == 1 ? string.Empty : "s")}");
            }
            else if (span.Seconds > 0)
            {
                countdownBuilder.Append($"{span.Seconds} second{(span.Seconds == 1 ? string.Empty : "s")}");
            }
            else
            {
                countdownBuilder.Clear();
            }

            return countdownBuilder.ToString();
        }
    }
}
