﻿using AudlyClient.Navigation.Services;
using Microsoft.AspNetCore.Components;
using System;
using Flurl;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudlyClient.Navigation;
using AudlyClient.Enums;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using AudlyClient.Services.Authentication;

namespace AudlyClient.Components.Authorization
{
    public partial class RedirectToLogin
    {
        [Inject]
        private IAuthenticationManager AuthenticationManager { get; set; }

        protected override void OnInitialized()
        {
            this.AuthenticationManager.StartLogin();
        }
    }
}
