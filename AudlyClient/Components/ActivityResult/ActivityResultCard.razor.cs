﻿using AudlyClient.Enums;
using AudlyClient.Extensions;
using AudlyClient.Models;
using AudlyClient.Models.Components;
using AudlyClient.Navigation;
using AudlyClient.Services.Knowledge;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.ActivityResult
{
    public partial class ActivityResultCard
    {
        [Parameter]
        public Activity Activity { get; set; }

        [Parameter]
        public bool ShowTopImage { get; set; } = true;

        [Parameter]
        public bool ShowPlaceholder { get; set; }

        [Parameter]
        public bool IsInProgress { get; set; }

        [Parameter]
        public RenderFragment LowerContent { get; set; }

        private string CardAccentColorClass =>
            $"{(this.IsInProgress ? "green" : string.Empty)}";

        private string GetImageUrl()
        {
            var imagesComponent = this.Activity.GetComponent<ImagesComponent>();

            var imageUrl = imagesComponent.ImageUrls.FirstOrDefault();

            if (string.IsNullOrEmpty(imageUrl))
                imageUrl = PlaceholderConstants.PLACEHOLDER_IMAGE_URL;

            return imageUrl;
        }
    }
}
