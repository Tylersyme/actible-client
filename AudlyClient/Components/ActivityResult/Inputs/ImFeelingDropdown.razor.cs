﻿using AudlyClient.ComponentData;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.ActivityResult.Inputs
{
    public partial class ImFeelingDropdown
    {
        private IReadOnlyList<ActivityFilterOption> _imFeelingOptions = ActivityFilterOption.ALL;

        [Parameter]
        public EventCallback<ActivityFilterOption> OnOptionSelected { get; set; }

        private async Task OnOptionClickedAsync(ActivityFilterOption imFeelingOption) =>
            await this.OnOptionSelected.InvokeAsync(imFeelingOption);

    }
}
