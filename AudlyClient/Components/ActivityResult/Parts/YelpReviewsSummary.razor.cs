﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.ActivityResult.Parts
{
    public partial class YelpReviewsSummary
    {
        [Parameter]
        public float Rating { get; set; }

        [Parameter]
        public string SourceUrl { get; set; }

        [Parameter]
        public string PriceLevelTitle { get; set; }

        [Parameter]
        public int TotalReviews { get; set; }
    }
}
