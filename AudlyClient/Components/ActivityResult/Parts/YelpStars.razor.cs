﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.ActivityResult.Parts
{
    public partial class YelpStars
    {
        [Parameter]
        public float Rating { get; set; }

        [Parameter]
        public int StarSize { get; set; } = 1;

        private string GetStarsImageFileNameFromRating()
        {
            var ratingFloor = Math.Floor(this.Rating);

            return this.GetStarsImageFileName((int)ratingFloor, Math.Round(ratingFloor) != this.Rating, this.StarSize);
        }

        private string GetStarsImageFileName(int ratingNumber, bool isHalf, int size = 1) =>
            $"regular_{ratingNumber}{(isHalf ? "_half" : string.Empty)}{(size > 1 ? $"@{size}x" : string.Empty)}.png";
    }
}
