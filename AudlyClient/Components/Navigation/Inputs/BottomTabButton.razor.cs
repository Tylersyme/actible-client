﻿using AudlyClient.Navigation.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Navigation.Inputs
{
    public partial class BottomTabButton
    {
        [Parameter]
        public string Text { get; set; }

        [Parameter]
        public string NavigateTo { get; set; }

        [Parameter]
        public string NavigateToContainer { get; set; }

        [Parameter]
        public string IconClass { get; set; }

        [Inject]
        private INavigationService NavigationService { get; set; }

        private void OnClick()
        {
            if (!string.IsNullOrEmpty(this.NavigateTo))
                this.NavigationService.NavigateTo(this.NavigateTo);
            else
                this.NavigationService.NavigateToContainer(this.NavigateToContainer);
        }
    }
}
