﻿using AudlyClient.ComponentData.Context;
using AudlyClient.Navigation.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Navigation
{
    public partial class NavigationBar
    {
        [Parameter]
        public string Title { get; set; }

        [Parameter]
        public bool HideNavigationArrow { get; set; }

        [Parameter]
        public EventCallback OnBackClicked { get; set; }

        [Parameter]
        public RenderFragment RightSideContent { get; set; }

        private async Task OnBackClickedAsync() =>
            await this.OnBackClicked.InvokeAsync(null);
    }
}
