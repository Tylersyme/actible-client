﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Components.Navigation
{
    public partial class BottomTabsNavbar
    {
        [Parameter]
        public RenderFragment ChildContent { get; set; }
    }
}
