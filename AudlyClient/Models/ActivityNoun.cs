﻿using AudlyClient.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class ActivityNoun
    {
        public ActivityNounType Type { get; set; }

        public IEnumerable<string> Adjectives { get; set; } = new List<string>();
    }
}
