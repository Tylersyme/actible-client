﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public static class ActivityTag
    {
        public const string ACTIVE = "active";
        public const string FOOD = "food";
        public const string WALK_AND_VIEW = "walk_and_view";
        public const string SHOW = "show";
        public const string RELAXING = "relaxing";
        public const string BEAUTY = "beauty";
    }
}
