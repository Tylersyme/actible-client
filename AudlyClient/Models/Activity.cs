﻿using AudlyClient.Enums;
using AudlyClient.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class Activity
    {
        /// <summary>
        /// Gets or sets the source of the activity's data.
        /// </summary>
        public ActivitySource Source { get; set; }

        /// <summary>
        /// Gets or sets the primary action that would be done with this activity.
        /// </summary>
        public ActivityVerb Verb { get; set; }

        /// <summary>
        /// Gets or sets the object or place which the <see cref="Verb"/> applies to.
        /// <para>For example, a verb of Eat and a noun of Restaurant are a possible combination.</para>
        /// </summary>
        public ActivityNoun Noun { get; set; }

        /// <summary>
        /// Gets or sets the user friendly title of the activity.
        /// </summary>
        public string Title { get; set; }

        public List<Component> Components { get; set; } = new List<Component>();
    }
}
