﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class AuthenticatedUser
    {
        public string Token { get; set; }

        public Guid UserId { get; set; }
    }
}
