﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class SignUp
    {
        [Required]
        public string Email { get; set; }

        public string Username { get; set; }

        [Required]
        public string PlainTextPassword { get; set; }
    }
}
