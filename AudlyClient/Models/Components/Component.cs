﻿using AudlyClient.Enums;
using AudlyClient.Services.Converters.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AudlyClient.Models.Components
{
    [JsonConverter(typeof(ComponentJsonConverter))]
    public abstract class Component
    {
        public virtual ComponentType Type { get; set; }
    }
}
