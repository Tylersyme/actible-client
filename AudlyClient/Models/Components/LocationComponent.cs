﻿using AudlyClient.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models.Components
{
    public class LocationComponent : Component
    {
        public override ComponentType Type =>
            ComponentType.Location;

        public string City { get; set; }

        public string Country { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public IEnumerable<string> DisplayAddresses { get; set; } = new List<string>();
    }
}
