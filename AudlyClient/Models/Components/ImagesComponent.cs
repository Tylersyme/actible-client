﻿using AudlyClient.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models.Components
{
    public class ImagesComponent : Component
    {
        public override ComponentType Type => ComponentType.Images;

        public List<string> ImageUrls { get; set; } = new List<string>();
    }
}
