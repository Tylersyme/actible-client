﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class Login
    {
        public string UsernameOrEmail { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string PlainTextPassword { get; set; }
    }
}
