﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class SavedActivity
    {
        public Activity Activity { get; set; }

        public SavedActivityDetails SavedActivityDetails { get; set; }
    }
}
