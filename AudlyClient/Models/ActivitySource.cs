﻿using AudlyClient.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class ActivitySource
    {
        /// <summary>
        /// Gets or sets the unique identifier for the activity summary.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the source from which all data was originally retrieved.
        /// </summary>
        public ActivitySourceType ActivitySourceType { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of milliseconds the activity source allows.
        /// </summary>
        public long MaxCacheDurationMilliseconds { get; set; }
    }
}
