﻿using AudlyClient.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Models
{
    public class ActivityVerb
    {
        public ActivityVerbType Type { get; set; }
    }
}
