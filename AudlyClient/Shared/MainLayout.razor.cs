﻿using AudlyClient.ComponentData.Context;
using AudlyClient.Navigation.Services.EventArgs;
using Easy.MessageHub;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Shared
{
    public partial class MainLayout
    {
        [Inject]
        private NavigationBarContext NavigationBarContext { get; set; }

        [Inject]
        private NavigationManager NavigationManager { get; set; }

        [Inject]
        private IMessageHub MessageHub { get; set; }

        protected override void OnInitialized()
        {
            this.NavigationBarContext.UpdatedEvent += () => base.StateHasChanged();

            // HACK: Used to initially refresh the navigation bar with the starting page
            // In the future, when the NavigationManager publishes an event for page initial
            // loads and refreshes, that event should be used instead. This would likely
            // go in NavigationService
            this.MessageHub.Publish(new PreNavigationEventArgs
            {
                Location = this.NavigationManager.BaseUri,
                IsBackwards = false,
            });
        }
    }
}
