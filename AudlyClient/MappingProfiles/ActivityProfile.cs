﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AudlyClient.Extensions;
using AudlyClient.Models;
using AudlyClient.Models.Components;
using AudlyClient.Requests;
using AutoMapper;

namespace AudlyClient.MappingProfiles
{
    public class ActivityProfile : Profile
    {
        public ActivityProfile()
        {
            base
                .CreateMap<Activity, ActivityDetailsRequest>()
                .ForMember(dest => dest.Latitude, opts => opts.MapFrom(src => src.GetComponent<PlaceInfoComponent>().Coordinates.Latitude))
                .ForMember(dest => dest.Longitude, opts => opts.MapFrom(src => src.GetComponent<PlaceInfoComponent>().Coordinates.Longitude))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Title));
        }
    }
}
