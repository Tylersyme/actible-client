﻿using AudlyClient.Common;
using AudlyClient.ComponentData.EventArgs;
using AudlyClient.Models;
using AudlyClient.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.MappingProfiles
{
    public class ViewModelProfile : Profile
    {
        public ViewModelProfile()
        {
            base.CreateMap<SavedActivityDetails, SavedActivityDetailsViewModel>();
            base.CreateMap<SavedActivityDetailsViewModel, SavedActivityDetails>();

            base.CreateMap<LocationViewModel, LocationDetails>();
            base.CreateMap<LocationDetails, LocationViewModel>();
        }
    }
}
