﻿using AudlyClient.Navigation;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient
{
    public static class RouteConfiguration
    {
        public static void ConfigureRoutes(ContainerBuilder builder)
        {
            builder
                .Register((_) => new RouteDefinition(Routes.HOME)
                    .WithRouteData(RouteDataKeys.NAVIGATION_BAR, new NavigationBarRouteData
                    {
                        Title = "Home",
                        UsesRightSideActionButton = true,
                    }))
                .AsSelf()
                .SingleInstance();

            builder
                .Register((_) => new RouteDefinition(Routes.EXPLORE)
                    .WithRouteData(RouteDataKeys.NAVIGATION_BAR, new NavigationBarRouteData
                    {
                        Title = "Explore",
                        UsesRightSideActionButton = true,
                    }))
                .AsSelf()
                .SingleInstance();

            builder
                .Register((_) => new RouteDefinition(Routes.SETUP_ACTIVITY)
                    .WithRouteData(RouteDataKeys.NAVIGATION_BAR, new NavigationBarRouteData
                    {
                        HideBottomTabs = true,
                        UsesRightSideActionButton = true,
                    }))
                .AsSelf()
                .SingleInstance();

            builder
                .Register((_) => new RouteDefinition(Routes.ACTIVITIES)
                    .WithRouteData(RouteDataKeys.NAVIGATION_BAR, new NavigationBarRouteData
                    {
                        Title = "Activities",
                    }))
                .AsSelf()
                .SingleInstance();

            builder
                .Register((_) => new RouteDefinition($"{Routes.AUTHENTICATION}/{{action}}{{?returnUrl}}")
                    .WithRouteData(RouteDataKeys.NAVIGATION_BAR, new NavigationBarRouteData
                    {
                        Title = "Account",
                        HideBottomTabs = true,
                        HideNavigationArrow = true,
                    }))
                .AsSelf()
                .SingleInstance();

            builder
                .Register((_) => new RouteDefinition(Routes.PROFILE)
                    .WithRouteData(RouteDataKeys.NAVIGATION_BAR, new NavigationBarRouteData
                    {
                        Title = "Profile",
                    }))
                .AsSelf()
                .SingleInstance();

            builder
                .Register((_) => new RouteDefinition("/activities{/activityId}")
                    .WithRouteData(RouteDataKeys.NAVIGATION_BAR, new NavigationBarRouteData
                    {
                        Title = "Plan",
                    }))
                .AsSelf()
                .SingleInstance();
        }
    }
}
