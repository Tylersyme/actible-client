﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Enums
{
    public class FormatConstants
    {
        public static readonly string[] EQUIVALENT_TIME_FORMATS = new string[]
        {
            "hh:mmtt",
            "h:mmtt",
        };

        public const string DATE_FORMAT = "dddd, d MMMM yyyy";

        public const string INPUT_TIME_FORMAT = "hh:mmtt";

        public const string TIME_FORMAT = "h:mm tt";

        public const string DATE_TIME_FORMAT_JAVASCRIPT = "dddd, d mmmm yyyy";
    }
}
