﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Enums
{
    public static class PlaceholderConstants
    {
        public const string PLACEHOLDER_IMAGE_URL = "https://medifactia.com/wp-content/uploads/2018/01/placeholder.png";
    }
}
