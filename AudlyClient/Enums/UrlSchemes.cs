﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Enums
{
    public static class UrlSchemes
    {
        public const string HTTPS = "https://";

        public const string HTTP = "http://";

        public const string MAPS = "maps://";
    }
}
