﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Enums
{
    public enum ActivityVerbType
    {
        Eat,
        Hike,
        Shop,
    }
}
