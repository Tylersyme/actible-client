﻿using AudlyClient.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Enums
{
    public class LazyPlaceReviewsComponent : Component
    {
        public override ComponentType Type => ComponentType.LazyPlaceReviews;
    }
}
