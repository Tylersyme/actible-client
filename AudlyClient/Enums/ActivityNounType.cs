﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Enums
{
    public enum ActivityNounType
    {
        Home,
        BoardGame,

        AmusementPark,
        Bar,
        Beach,
        Bowling,
        CafePub,
        CampingGround,
        Casino,
        ComedyClub,
        Cinema,
        Garden,
        WildlifePark,
        SportsCourt,
        Park,
        PanoramicView,
        PlaceOfWorship,
        RecreationCenter,
        Store,
        Convention,
        GolfCourse,
        IceSkating,
        TouristAttraction,
        Theater,
        Library,
        Club,
        Trail,
        Restaurant,
        Spa,
        SportsCenter,
        Stadium,
        SwimmingPool,
        Outdoors,
        Winery,
        LeisureCenter,
        Museum,
        Zoo,
    }
}
