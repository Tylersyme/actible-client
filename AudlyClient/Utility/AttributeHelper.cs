﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Utility
{
    public static class AttributeHelper
    {
        public static IDictionary<string, object> Create(params (string name, object value)[] attributes)
        {
            var attributeDictionary = new Dictionary<string, object>();

            foreach (var attribute in attributes)
                attributeDictionary.Add(attribute.name, attribute.value);

            return attributeDictionary;
        }
    }
}
