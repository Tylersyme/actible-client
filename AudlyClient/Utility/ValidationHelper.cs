﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Utility
{
    public static class ValidationHelper
    {
        public static bool IsValidEmail(string value) =>
            new EmailAddressAttribute().IsValid(value);
    }
}
