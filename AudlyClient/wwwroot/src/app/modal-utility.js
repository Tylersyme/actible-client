﻿
window.openModal = (selector, hideBackdrop) => {
  const element = $(selector);

  element.modal();

  if (hideBackdrop) {
    $('.modal-backdrop').css('background-color', 'transparent');
  }
};

window.closeModal = (selector) => {
  const element = $(selector);

  element.modal('hide');
};