﻿
window.initializeStepper = (selector) => {
  const element = $(selector);

  element.mdbStepper();
};

window.initializeSelects = (selector) => {
  const elements = $(selector);

  elements.materialSelect();
};

window.initializeDatepicker = (datepickerSelector, inputSelector, dotnetHelper, callbackName, datepickerOptionsJson) => {
  try {
    const element = $(datepickerSelector);

    const datepickerOptions = JSON.parse(datepickerOptionsJson);

    element
      .datepicker(datepickerOptions)
      .on('change', () => {
        const inputElement = $(inputSelector);

        dotnetHelper.invokeMethodAsync(callbackName, inputElement.val());
      });

    const inputElement = $(inputSelector);

    inputElement.on('click', (e) => {
      inputElement
        .parent()
        .find('i')
        .last()
        .trigger('click');

      e.preventDefault();
      e.stopPropagation();
    });

  } catch(e) {
    // TODO: Figure out why an error occurs every second initialization, it does not appear to do anything negative
    console.warn(e);
  }
};

window.initializeTimepicker = (selector, inputSelector, dotnetHelper, callbackName) => {
  $(selector).timepicker();

  const inputElement = $(inputSelector);

  inputElement.on('change', () => {
    dotnetHelper.invokeMethodAsync(callbackName, inputElement.val());
  });

  inputElement.on('click', (e) => {
    inputElement
      .parent()
      .find('i')
      .last()
      .trigger('click');

    e.preventDefault();
    e.stopPropagation();
  });
};

window.mdbStepper = {
  step: (selector) => {
    const element = $(selector);

    element.nextStep();
  },
};

window.setAutocompleteDataAndShow = (selector, autocompleteOptions) => {
  const element = $(selector);

  // MD Bootstrap does not remove old autocomplete window by default
  // This ensures that the new data is all that is shown
  element
    .parent()
    .find('.mdb-autocomplete-wrap')
    .remove();

  element.mdbAutocomplete({
    data: autocompleteOptions
  });

  // Shows the autocomplete results
  element.focus();
};
