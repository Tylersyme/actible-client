﻿
window.getGeolocationCoordinates = (dotnetHelper, successCallback, errorCallback) =>
  navigator.geolocation.getCurrentPosition(
    (position) => {
      console.log('Location Found');

      dotnetHelper.invokeMethodAsync(successCallback, window.cloneAsObject(position.coords));
    },
    (error) => {
      console.log(`Error getting location. Error code: ${error.code}`);

      dotnetHelper.invokeMethodAsync(errorCallback);
    }
  );