﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient
{
    public static class Constants
    {
#if DEBUG
        public const string API_BASE_ADDRESS = "https://localhost:5004/";
#else
        public const string API_BASE_ADDRESS = "https://actibleapi.azurewebsites.net";
#endif
    }
}
