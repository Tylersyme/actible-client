﻿using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ViewModels
{
    public class LocationViewModel
    {
        public GeolocationCoordinates Coordinates { get; set; }

        public string PlaceName { get; set; }
    }
}
