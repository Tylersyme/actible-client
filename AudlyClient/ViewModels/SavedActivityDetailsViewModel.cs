﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.ViewModels
{
    public class SavedActivityDetailsViewModel
    {
        public DateTime StartTime { get; set; }

        public string Instructions { get; set; }
    }
}
