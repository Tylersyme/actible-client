﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Helpers
{
    public static class MDBootstrapAnimationHelper
    {
        private const string ANIMATED_CLASS = "animated";

        public static string GetAnimationClasses(params string[] mdBootstrapAnimations) =>
            $"{ANIMATED_CLASS} {string.Join(' ', mdBootstrapAnimations)}";
    }

    public static class MDBootstrapAnimations
    {
        public const string FAST = "fast";
        public const string FASTER = "faster";

        public const string FADE_OUT_LEFT = "fadeOutLeft";
        public const string FADE_OUT_RIGHT = "fadeOutRight";
        public const string FADE_IN_LEFT = "fadeOutLeft";
    }
}
