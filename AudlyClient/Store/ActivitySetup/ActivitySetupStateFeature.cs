﻿using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ActivitySetup
{
    public class ActivitySetupStateFeature : Feature<ActivitySetupState>
    {
        public override string GetName() =>
            "CreatedActivity";

        protected override ActivitySetupState GetInitialState() =>
            new ActivitySetupState(null, null);
    }
}
