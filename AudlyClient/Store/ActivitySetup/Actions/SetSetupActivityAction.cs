﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ActivitySetup.Actions
{
    public class SetSetupActivityAction
    {
        public Activity Activity { get; set; }
    }
}
