﻿using AudlyClient.Store.ActivitySetup.Actions;
using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ActivitySetup.Reducers
{
    public static class ActivitySetupReducers
    {
        [ReducerMethod]
        public static ActivitySetupState ReduceSetSetupActivityAction(ActivitySetupState state, SetSetupActivityAction action) =>
            new ActivitySetupState(action.Activity, state.SavedActivityDetails);

        [ReducerMethod]
        public static ActivitySetupState ReduceSetSavedActivityDetailsAction(ActivitySetupState state, SetSavedActivityDetailsAction action) =>
            new ActivitySetupState(state.Activity, action.SavedActivityDetails);
    }
}
