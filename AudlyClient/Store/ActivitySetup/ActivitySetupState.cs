﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ActivitySetup
{
    public class ActivitySetupState
    {
        public Activity Activity { get; }

        public SavedActivityDetails SavedActivityDetails { get; }

        public ActivitySetupState(Activity activity, SavedActivityDetails savedActivityDetails) =>
            (this.Activity, this.SavedActivityDetails) = (activity, savedActivityDetails);
    }
}
