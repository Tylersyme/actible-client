﻿using AudlyClient.Common;
using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ExploreActivities.Actions
{
    public class SetLocationBiasAction
    {
        public LocationDetails LocationBias { get; set; }
    }
}
