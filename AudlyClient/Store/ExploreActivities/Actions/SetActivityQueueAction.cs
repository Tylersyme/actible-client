﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ExploreActivities.Actions
{
    public class SetActivityQueueAction
    {
        public List<Activity> ActivityQueue { get; set; } = new List<Activity>();
    }
}
