﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ExploreActivities.Actions
{
    public class SetCurrentActivityAction
    {
        public Activity CurrentActivity { get; set; }
    }
}
