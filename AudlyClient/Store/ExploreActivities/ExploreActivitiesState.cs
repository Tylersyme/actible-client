﻿using AudlyClient.Common;
using AudlyClient.Models;
using AudlyClient.Services.Interop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ExploreActivities
{
    public class ExploreActivitiesState
    {
        public LocationDetails LocationBias { get; set; }

        public List<Activity> ActivityQueue { get; set; }

        public Activity CurrentActivity { get; set; }

        public ExploreActivitiesState(
            LocationDetails locationBias,
            List<Activity> activityQueue,
            Activity currentActivity)
        {
            this.LocationBias = locationBias;
            this.ActivityQueue = activityQueue;
            this.CurrentActivity = currentActivity;
        }
    }
}
