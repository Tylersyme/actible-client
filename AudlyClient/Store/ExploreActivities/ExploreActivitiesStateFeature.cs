﻿using AudlyClient.Models;
using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ExploreActivities
{
    public class ExploreActivitiesStateFeature : Feature<ExploreActivitiesState>
    {
        public override string GetName() =>
            "ExploreActivities";

        protected override ExploreActivitiesState GetInitialState() =>
            new ExploreActivitiesState(null, new List<Activity>(), null);
    }
}
