﻿using AudlyClient.Store.ExploreActivities.Actions;
using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.ExploreActivities.Reducers
{
    public static class ExploreActivitiesReducers
    {
        [ReducerMethod]
        public static ExploreActivitiesState ReduceSetLocationBiasAction(ExploreActivitiesState state, SetLocationBiasAction action) =>
            new ExploreActivitiesState(action.LocationBias, state.ActivityQueue, state.CurrentActivity);

        [ReducerMethod]
        public static ExploreActivitiesState ReduceSetActivityQueueAction(ExploreActivitiesState state, SetActivityQueueAction action) =>
            new ExploreActivitiesState(state.LocationBias, action.ActivityQueue, state.CurrentActivity);

        [ReducerMethod]
        public static ExploreActivitiesState ReduceSetCurrentActivityAction(ExploreActivitiesState state, SetCurrentActivityAction action) =>
            new ExploreActivitiesState(state.LocationBias, state.ActivityQueue, action.CurrentActivity);
    }
}
