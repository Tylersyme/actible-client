﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.SelectedActivity.Actions
{
    public class SetSelectedActivityAction
    {
        public Activity Activity { get; set; }
    }
}
