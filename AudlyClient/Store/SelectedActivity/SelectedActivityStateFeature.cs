﻿using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.SelectedActivity
{
    public class SelectedActivityStateFeature : Feature<SelectedActivityState>
    {
        public override string GetName() =>
            "SelectedActivity";

        protected override SelectedActivityState GetInitialState() =>
            new SelectedActivityState(null);
    }
}
