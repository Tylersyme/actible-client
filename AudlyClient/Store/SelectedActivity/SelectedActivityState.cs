﻿using AudlyClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.SelectedActivity
{
    public class SelectedActivityState
    {
        public Activity Activity { get; }

        public SelectedActivityState(Activity activity) =>
            this.Activity = activity;
    }
}
