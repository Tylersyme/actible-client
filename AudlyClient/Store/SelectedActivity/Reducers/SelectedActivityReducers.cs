﻿using AudlyClient.Store.SelectedActivity.Actions;
using Fluxor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Store.SelectedActivity.Reducers
{
    public static class SelectedActivityReducers
    {
        [ReducerMethod]
        public static SelectedActivityState ReduceSetSelectedActivityAction(SelectedActivityState state, SetSelectedActivityAction action) =>
            new SelectedActivityState(action.Activity);
    }
}
