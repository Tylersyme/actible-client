﻿using AudlyClient.ComponentData.Context;
using AudlyClient.Extensions;
using AudlyClient.Models;
using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using AudlyClient.Services;
using AudlyClient.Services.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Pages
{
    [Authorize]
    public partial class Index
    {
        [CascadingParameter]
        public NavigationBarContext NavigationBarContext { get; set; }

        private List<SavedActivity> ScheduledActivities { get; set; } = new List<SavedActivity>();

        /// <summary>
        /// Used to transition from the begins countdown to the ends countdown in real time. 
        /// </summary>
        private bool _forceShowEndCountdown = false;

        [Inject]
        private ISavedActivityService SavedActivityService { get; set; }

        [Inject]
        private IAuthenticationManager AuthenticationManager { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.NavigationBarContext.RightSideActionButtonText = "Switch Account";
            this.NavigationBarContext.OnRightSideActionButtonClicked = () => this.AuthenticationManager.StartLogout();

            this.ScheduledActivities = (await this
                .SavedActivityService.GetSavedActivitiesAsync())
                .GetScheduledActivitiesOrderedByDate()
                .ToList();
        }

        private void CountdownCompleted()
        {
            _forceShowEndCountdown = true; 

            base.StateHasChanged();
        }
    }
}
