﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Pages.Authentication
{
    public partial class Authentication
    {
        [Parameter]
        public string Action { get; set; }
    }
}
