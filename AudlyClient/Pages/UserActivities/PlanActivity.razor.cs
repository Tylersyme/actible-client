﻿using AudlyClient.Components.Modals;
using AudlyClient.Enums;
using AudlyClient.Extensions;
using AudlyClient.Models;
using AudlyClient.Models.Components;
using AudlyClient.Services;
using AudlyClient.Services.Interop;
using AudlyClient.Services.Knowledge;
using AudlyClient.Services.Utility;
using AudlyClient.Store.SelectedActivity;
using AudlyClient.ViewModels;
using AutoMapper;
using Fluxor;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Pages.UserActivities
{
    [Authorize]
    public partial class PlanActivity
    {
        [Parameter]
        public string ActivityIdentifier { get; set; }

        [Inject]
        private ISavedActivityService SavedActivityService { get; set; }

        [Inject]
        private IMapper Mapper { get; set; }

        [Inject]
        private IMapsService MapsService { get; set; }

        private SavedActivity _savedActivity;

        private EditActivityStartTimeModal _editActivityStartTimeModal;

        private string _mapsUrl;

        private bool _isLoading = true;

        protected override async Task OnInitializedAsync()
        {
            _savedActivity = (await this.SavedActivityService
                .GetSavedActivitiesAsync())
                .FirstOrDefault(a => a.Activity.Source.Identifier == this.ActivityIdentifier);

            _mapsUrl = await this.GetMapsUrlAsync();

            _isLoading = false;
        }

        private string GetTitle()
        {
            if (_savedActivity == null)
                return default;

            return _savedActivity.Activity.Title;
        }

        private float GetRating()
        {
            var placeReviewsComponent = _savedActivity.Activity.GetComponent<PlaceReviewsComponent>();

            return placeReviewsComponent.Rating;
        }

        private string GetReviewSourceUrl()
        {
            var placeReviewsComponent = _savedActivity.Activity.GetComponent<PlaceReviewsComponent>();

            return placeReviewsComponent.ReviewSourceUrl;
        }

        private int GetReviewCount()
        {
            var placeReviewsComponent = _savedActivity.Activity.GetComponent<PlaceReviewsComponent>();

            return placeReviewsComponent.ReviewCount;
        }

        private LocationComponent GetLocationComponent() =>
            _savedActivity.Activity.GetComponent<LocationComponent>();

        private async Task<string> GetMapsUrlAsync()
        {
            var placeInfoComponent = _savedActivity.Activity.GetComponent<PlaceInfoComponent>();

            var mapUrl = await this.MapsService.GetPlatformSpecificMapUrlAsync(
                placeInfoComponent.Coordinates.Latitude, 
                placeInfoComponent.Coordinates.Longitude);

            return mapUrl;
        }

        private SavedActivityDetailsViewModel GetSavedActivityDetailsViewModel() =>
            this.Mapper.Map<SavedActivityDetailsViewModel>(_savedActivity.SavedActivityDetails);

        private async Task EditSavedActivityDetailsAsync(SavedActivityDetailsViewModel viewModel)
        {
            this.Mapper.Map(viewModel, _savedActivity.SavedActivityDetails);

            await this.SavedActivityService.UpdateSavedActivityAsync(_savedActivity);
        }
    }
}
