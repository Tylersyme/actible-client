﻿using AudlyClient.Models;
using AudlyClient.Store.SelectedActivity;
using Fluxor;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Pages.UserActivities
{
    [Authorize]
    public partial class SetupActivity
    {
        [Inject]
        private IState<SelectedActivityState> SelectedActivityState { get; set; }

        private Activity Activity =>
            this.SelectedActivityState.Value.Activity;
    }
}
