﻿using AudlyClient.Components.Modals;
using AudlyClient.Extensions;
using AudlyClient.Models;
using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using AudlyClient.Services;
using AudlyClient.Services.Clients;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Pages.UserActivities
{
    [Authorize]
    public partial class Activities
    {
        private ConfirmDeleteModal _confirmDeleteModal;

        private string _confirmDeleteActivityModalId = Guid.NewGuid().ToString();

        private Activity _toDeleteOnConfirmation;

        private List<SavedActivity> ScheduledActivities { get; set; } = new List<SavedActivity>();

        private List<SavedActivity> SavedForLaterActivities { get; set; } = new List<SavedActivity>();

        [Inject]
        private ISavedActivityService SavedActivityService { get; set; }

        [Inject]
        private IActivityNavigationService ActivityNavigationService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await this.RefreshActivitiesAsync();
        }

        private void OnPlanClicked(Activity activity) =>
            this.ActivityNavigationService.NavigateToPlanActivity(activity);

        private async Task RemoveConfirmedAsync()
        {
            await this.SavedActivityService.DeleteSavedActivityAsync(_toDeleteOnConfirmation.Source.Identifier);

            await this.RefreshActivitiesAsync();
        }

        private async Task RefreshActivitiesAsync()
        {
            var savedActivities = await this.SavedActivityService.GetSavedActivitiesAsync();

            this.ScheduledActivities = savedActivities
                .GetScheduledActivitiesOrderedByDate()
                .ToList();

            this.SavedForLaterActivities = savedActivities
                .Where(a => !a.IsScheduled())
                .OrderBy(a => a.Activity.Title)
                .ToList();
        }

        private async Task ShowDeleteActivityConfirmationAsync(Activity activity)
        {
            _toDeleteOnConfirmation = activity;

            await _confirmDeleteModal.OpenAsync();
        }
    }
}
