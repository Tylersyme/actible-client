﻿using AudlyClient.Common;
using AudlyClient.ComponentData;
using AudlyClient.ComponentData.Context;
using AudlyClient.Components.Modals;
using AudlyClient.Extensions.Utility;
using AudlyClient.Helpers;
using AudlyClient.Models;
using AudlyClient.Navigation;
using AudlyClient.Navigation.Services;
using AudlyClient.Requests;
using AudlyClient.Services.Clients;
using AudlyClient.Services.Interop;
using AudlyClient.Services.Interop.Models;
using AudlyClient.Services.Utility;
using AudlyClient.Store.ExploreActivities;
using AudlyClient.Store.ExploreActivities.Actions;
using AudlyClient.Store.SelectedActivity.Actions;
using AudlyClient.ViewModels;
using AutoMapper;
using Fluxor;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyClient.Pages
{
    [Authorize]
    public partial class Explore
    {
        [CascadingParameter]
        public NavigationBarContext NavigationBarContext { get; set; }

        /// <summary>
        /// The minimum number of activities in the queue before more should be loaded.
        /// </summary>
        private const int ACTIVITY_BUFFER_AMOUNT = 5;

        private readonly string _id = Guid.NewGuid().ToString();

        private ActivityFilterOption _selectedFilterOption;

        private bool _animateToActivityAccepted = false;

        private string ActivityAcceptedTransitionOutAnimationClasses =>
            _animateToActivityAccepted
                ? MDBootstrapAnimationHelper.GetAnimationClasses(MDBootstrapAnimations.FADE_OUT_RIGHT, MDBootstrapAnimations.FASTER)
                : string.Empty;

        private bool IsLocationSelected =>
            this.ExploreActivitiesState.Value.LocationBias?.Coordinates != null;

        private LocationModal _locationModal;

        private bool _isLoadingActivities = false;

        private string _activityFilterModalId = "filter-activity-modal";

        private Activity CurrentActivity
        {
            get => this.ExploreActivitiesState.Value.CurrentActivity;
            set => this.Dispatcher.Dispatch(new SetCurrentActivityAction { CurrentActivity = value });
        }

        private List<Activity> ActivityQueue
        {
            get => this.ExploreActivitiesState.Value.ActivityQueue;
            set => this.Dispatcher.Dispatch(new SetActivityQueueAction { ActivityQueue = value });
        }

        [Inject]
        private IActivityClient ActivityClient { get; set; }

        [Inject]
        private IMapper Mapper { get; set; }

        [Inject]
        private IDispatcher Dispatcher { get; set; }

        [Inject]
        private IJSRuntime JSRuntime { get; set; }

        [Inject]
        private ILocationService LocationService { get; set; }

        [Inject]
        private IModalInteropService ModalInteropService { get; set; }

        [Inject]
        private INavigationService NavigationService { get; set; }

        [Inject]
        private IMDBootstrapInteropService MDBootstrapInteropService { get; set; }

        [Inject]
        private IState<ExploreActivitiesState> ExploreActivitiesState { get; set; }

        protected override async Task OnInitializedAsync()
        {
            if (this.CurrentActivity == null && this.IsLocationSelected)
                await this.NextActivityAsync();

            if (this.IsLocationSelected)
                this.ShowFilterButton();
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender)
                return;

            await this.MDBootstrapInteropService.InitializeSelectsAsync();
        }

        private async Task NextActivityAsync()
        {
            await this.EnsureActivityBufferAsync();

            this.ActivityQueue.RemoveAt(0);

            this.CurrentActivity = this.ActivityQueue.First();
        }

        private void PlanActivityClicked()
        {
            this.NavigationService.NavigateTo(Routes.SETUP_ACTIVITY);

            this.Dispatcher.Dispatch(new SetSelectedActivityAction
            {
                Activity = this.CurrentActivity,
            });
        }

        private async Task LoadNextActivitiesToQueueAsync()
        {
            this.UpdateIsLoadingActivities();

            var activityRequest = new ActivityRequest();

            activityRequest.Tags = _selectedFilterOption?.AssociatedTags.ToList() ?? new List<string>();

            var locationBias = this.ExploreActivitiesState.Value.LocationBias;

            if (locationBias == null)
                return;

            activityRequest.GeolocationCoordinates = locationBias.Coordinates;

            var nextActivities = await this.ActivityClient.GetNextActivitiesAsync(activityRequest);

            this.ActivityQueue.AddRange(nextActivities);

            this.UpdateIsLoadingActivities();
        }

        [JSInvokable]
        public void NavigateToSetupActivityPage() =>
            this.NavigationService.NavigateTo(Routes.SETUP_ACTIVITY);

        private async Task OnLocationSubmitAsync(LocationViewModel locationViewModel)
        {
            var locationDetails = this.Mapper.Map<LocationDetails>(locationViewModel);

            this.Dispatcher.Dispatch(new SetLocationBiasAction
            {
                LocationBias = locationDetails,
            });

            await this.ReloadActivitiesAsync();

            this.ShowFilterButton();
        }

        private async Task SelectedFilterChangedAsync(ChangeEventArgs eventArgs)
        {
            _selectedFilterOption = ActivityFilterOption.ALL.FirstOrDefault(o => o.Name == eventArgs.Value.ToString());

            await this.ReloadActivitiesAsync();
        }

        /// <summary>
        /// Ensures that the activity queue has a buffer of activities.
        /// </summary>
        private async Task EnsureActivityBufferAsync()
        {
            if (this.IsActivityBufferSatisfied())
                return;

            await this.LoadNextActivitiesToQueueAsync();
        }

        private async Task ReloadActivitiesAsync()
        {
            this.ActivityQueue.Clear();

            await this.LoadNextActivitiesToQueueAsync();

            await this.NextActivityAsync();
        }

        private async Task ChangeSearchLocationClickedAsync()
        {
            await this.ModalInteropService.CloseModalAsync(_activityFilterModalId.ToCssId());

            await _locationModal.OpenAsync();
        }

        private bool IsActivityBufferSatisfied() =>
            this.ActivityQueue.Count >= ACTIVITY_BUFFER_AMOUNT;

        private void UpdateIsLoadingActivities() =>
            _isLoadingActivities = !this.ActivityQueue.Any();

        private void ShowFilterButton()
        {
            this.NavigationBarContext.RightSideActionButtonText = "Filter";
            this.NavigationBarContext.OnRightSideActionButtonClicked =
                async () => await this.ModalInteropService.OpenModalAsync(_activityFilterModalId.ToCssId(), true);
        }
    }
}
